<?php 

class Users

{
	Private $iduser;
	private $nomuser;
	private $prenomuser;
	private $emailuser;
	private $identifiantuser;
	private $password;
	private $statut;
	private $telephone;
	


	public function  __construct($id,$nomuser,$prenomuser,$emailuser,$identifiantuser,$password,$statut,$telephone){
		$this->iduser =$id;
		$this->nomuser =$nomuser;
		$this->prenomuser =$prenomuser;
		$this->emailuser =$emailuser;
		$this->identifiantuser =$identifiantuser;
		$this->password =$password;
		$this->statut =$statut;
		$this->telephone =$telephone;
	}



	// Les getters
	public function getIduser(){return $this->iduser;}
	public function getNomuser(){return $this->nomuser;}
	public function getPrenomuser(){return $this->prenomuser;} 
	public function getEmailuser(){return $this->emailuser;} 
	public function getIdentifiantuser(){return $this->identifiantuser;}
	public function getPassword(){return $this->password;}
	public function getStatut(){return $this->statut;}
	public function getTelephone(){return $this->telephone;}    
	
 
	// les setteurs
	public function setIduser($iduser)
	{
		$iduser = (int)$iduser;
		$this->iduser = $iduser;
	}

	public function setNomuser($nomuser)
	{
		$this->nomuser = $nomuser;
	}

	public function setPrenomuser($prenomuser)
	{
		$this->prenomuser = $prenomuser;
	
	}


	public function setEmailuser($email)
	{
		$this->email = $email;
	
	}
	

	public function setIdentifiantuser($identifiant)
	{
		$this->identifiant = $identifiant;
	
	}

	public function setPassword($password)
	{
		$this->password = $password;
	
	}

	public function setStatut($statut)
	{
		$this->statut = $statut;
	
	}
	public function setTelephone($telephone)
	{
		$this->telephone = $telephone;
	
	}
	
	

}


include('config.php');

//require_once("connexion.php");

class ManagerUsers
{
private $_db; // Instance de PDO
public function __construct($db)
{
$this->setDb($db);
}


public function setDb(PDO $db)
{
$this->_db = $db;

}

public function controle($identifiant, $password){ 

$q = $this->_db->query('SELECT * FROM users WHERE identifiantuser = "'.$identifiant.'" AND password = "'.$password.'" ');
$dah = $q->RowCount();
return $dah;
}
public function recupere($identifiant, $password){ 

$q = $this->_db->query('SELECT * FROM users WHERE identifiantuser = "'.$identifiant.'" AND password = "'.$password.'" ');
$dah = $q->fetch(PDO::FETCH_ASSOC);
return new Users($dah);
}

public function chercherlog($log)
{
             $req = array();
	    	$requete = (" SELECT* FROM users where identifiantuser='$log'");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	 				$req[]= new Users ($donnee['iduser'],$donnee['nomuser'],$donnee['prenomuser'],$donnee['emailuser'],$donnee['identifiantuser'],$donnee['password'],$donnee['statut'],$donnee['telephone']); 
	    	   
	    	}
	    	return $req;
   	
	    
}

public function ajouteruser(Users $u){
	    	
	       $req = $this->_db->prepare("
	                     INSERT INTO users (`iduser`, `nomuser`, `prenomuser`, `emailuser`, `identifiantuser`, `password`, `statut`, `telephone`) 
	                               VALUES (null,:ihc,:ihp,:ihpt,:hr,:con,:idc,:idco)");
	   
	    	/*$req-> bindValue(':id',$ccs->getIdEdm_a());*/
	    	$req-> bindValue(':ihc',$u->getNomuser());
	    	$req-> bindValue(':ihp',$u->getPrenomuser());
	    	$req-> bindValue(':ihpt',$u->getEmailuser());
	    	/*$req-> bindValue(':dr',$te->getDateReleve());*/
	    	$req-> bindValue(':hr',$u->getIdentifiantuser());
	    	$req-> bindValue(':con',$u->getPassword());
	    	$req-> bindValue(':idc',$u->getStatut());
	    	$req-> bindValue(':idco',$u->getTelephone());		    	
	    	
	    	$req->execute(); 

	    }
//=========================================================liste utilisateurs===================================================

 public function Afficher()
{
             $req = array();
	    	$requete = " SELECT* FROM users";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $req[] = new Users ($donnee['iduser'],$donnee['nomuser'],$donnee['prenomuser'],$donnee['emailuser'],$donnee['identifiantuser'],$donnee['password'],$donnee['statut'],$donnee['telephone']); 
	    	    
	    	   
	    	}
	    	return $req;    
}

//fonction login validate
public function login($loguser)
{
	$erreur=null;
$requete = (" SELECT* FROM users where identifiantuser='$loguser'");
	    	$list = $this->_db->query($requete);
	    	if ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {

	$erreur="<center><font color='red'><h3>Ce Pseudo existe deja choisissez un autre svp!</h3></font></center>";
	 
}//fin if
	return $erreur;	
}


	//recherche pour afficher un objet
		 public function chercher($users)
{
             $req = array();
	    	$requete = (" SELECT  *  FROM users where users.iduser=".$users);
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$req[] = new Users ($donnee['iduser'],$donnee['nomuser'],$donnee['prenomuser'],$donnee['emailuser'],$donnee['identifiantuser'],$donnee['password'],$donnee['statut'],$donnee['telephone']); 
	    	
	    	}
	    	return $req;   
}


 public function modifier($users) {

	    $req= $this->_db ->prepare("UPDATE users set nomuser= :id, prenomuser= :v, emailuser = :l,identifiantuser= :n ,password= :e 
	    	,telephone= :t/*,statut= :st*/ where iduser =".$users);

	    	$req-> bindValue(':id',$_POST['nom']);
	    	$req-> bindValue(':v',$_POST['prenom']);
	    	$req-> bindValue(':l',$_POST['email']);
	    	$req-> bindValue(':n',$_POST['identifiant']);
	    	$req-> bindValue(':e',$_POST['motdepasse']);
	    	$req-> bindValue(':t',$_POST['telephone']);
	    	/*$req-> bindValue(':st',$_POST['statut']);*/
			$req->execute();
		
		}

   //supprimer un objet 
public function supprimer($users)
{
$this->_db->exec("DELETE FROM users WHERE iduser=".$users);
}//fin supprimer*/


}
  

?>