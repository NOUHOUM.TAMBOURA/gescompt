
<?php


class managersomme
{
private $_db; // Instance de PDO
public function __construct($db)
{
$this->setDb($db);
}


public function setDb(PDO $db)
{
$this->_db = $db;

}
 
//=====================================================SOMAGEP===================================================
   public function sommeSomagep()
{
     return $this->_db->query('SELECT sum(consommation) from concomptsomagep')->fetchColumn();
}

   public function sommetesteurSomagep()
{
     return $this->_db->query('SELECT sum(consommation) from constesteursomagep')->fetchColumn();
}

   public function nombreCompteur()
{
     return $this->_db->query('select count(idcomptS) from comptsomagep')->fetchColumn();
}

//=====================================================EDM Numerique===================================================
   public function sommeEdm_n()
{
     return $this->_db->query('SELECT sum(consommation) from consedm_n')->fetchColumn();
}

   public function sommetesteurEdm_n()
{
     return $this->_db->query('SELECT sum(consommation) from constesteuredm_n')->fetchColumn();
}

   public function nombreEdm_n()
{
     return $this->_db->query('SELECT count(idedm_n) from comptedm_n')->fetchColumn();
}

//=========================================================EDM Analogique=================================================

   public function sommeEdm_a()
{
     return $this->_db->query('SELECT sum(consommation) from consedm_a')->fetchColumn();
}

   public function sommetesteurEdm_a()
{
     return $this->_db->query('SELECT sum(consommation) from constesteur_a')->fetchColumn();
}

   public function nombreEdm_a()
{
     return $this->_db->query('SELECT count(idedm_a) from comptedm_a')->fetchColumn();
}

	
//====================================================Electromenager=========================================================
	public function sommeElectro()
{
     return $this->_db->query('SELECT sum(index1) from consoelectromenager')->fetchColumn();
}

  public function nombreElectro()
{
     return $this->_db->query('SELECT count(idelectro) from creationelectromenager')->fetchColumn();
}
//=================================================Somme des Consommations =====================================

public function SommeConsoSomagep($date1,$date2)
{
                return $this->_db->query("SELECT sum(index1) ,sum(consommation) 
  	            from concomptsomagep where datereleve between '$date1' and '$date2'")->fetchColumn();
}
//====================================================== vehicule ==============================================================

/*  public function somme()
{
     return $this->_db->query('SELECT sum(index1) from consoelectromenager')->fetchColumn();
}*/

  public function nombreVehicule()
{
     return $this->_db->query('SELECT count(idVehicule) from vehicule')->fetchColumn();
}

//==================================================== generateur =====================================================================

  public function nombreGeneteur()
{
     return $this->_db->query('SELECT count(idGenerateur) from generateur')->fetchColumn();
}

}	

?>