<?php 

class Vehicule

{
	Private $idVehicule;
	private $plaque;
	private $modele;
	private $type;
	private $numeroSerie;
	private $proprietaire;
	

public function __construct($id,$p,$m,$t,$n,$pr){
		$this->idVehicule =$id;
		$this->plaque =$p;
		$this->modele =$m;
		$this->type =$t;
		$this->numeroSerie =$n;
		$this->proprietaire =$pr;
	}


	// Les getters
	public function getIdVehicule(){return $this->idVehicule;}
	public function getPlaque(){return $this->plaque;}
	public function getModele(){return $this->modele;}
	public function getType(){return $this->type;}
	public function getNumeroSerie(){return $this->numeroSerie;}
	public function getProprietaire(){return $this->proprietaire;} 
	
 
	// les setteurs
	public function setIdVehicule($idVehicule)
	{
		$this->idVehicule= $idVehicule;
	}
	public function setPlaque($plaque)
	{
		$this->plaque = $plaque;
		
	}
	public function setModele($modele)
	{

		$this->modele = $modele;
		
	}
	public function setType($type)
	{

		$this->type = $type;
	}
	public function setNumeroSerie($numeroSerie)
	{

		$this->numeroSerie = $numeroSerie;
	}
	public function setProprietaire($proprietaire)
	{

		$this->proprietaire = $proprietaire;
	}
	

}
//====================================================vehicule manager =======================================================

class ManagerVehicule
{
private $_db; // Instance de PDO
public function __construct($db)
{
$this->setDb($db);
}


public function setDb(PDO $db)
{
$this->_db = $db;
}




	  // fonction qui permet de verifier si identifiant existe dans la base
	    public function verifier($id)
{
	$erreur=null;
$requete = (" SELECT* FROM vehicule where plaque='$id'");

	    	$list = $this->_db->query($requete);
	    	if ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {

	$erreur="<center><font color='red'><h3>Ce Vehicule Existe deja!</h3></font></center>";
	
}//fin if
	return $erreur;	

}
    //Ajouter un compteur
	public function ajoutervehicule(Vehicule $vehicule){
	  
	       $req = $this->_db->prepare("
	                     INSERT INTO vehicule (`idVehicule`, `plaque`, `modele`, `type`, `numeroSerie`, `proprietaire`) 
	                               VALUES (null,:p,:m,:t,:n,:pr)");
	   
	    	$req-> bindValue(':p',$vehicule->getPlaque());
	    	$req-> bindValue(':m',$vehicule->getModele());
	    	$req-> bindValue(':t',$vehicule->getType());
	    	$req-> bindValue(':n',$vehicule->getNumeroSerie());
	    	$req-> bindValue(':pr',$vehicule->getProprietaire());	 	    	
	    	
	    	$req->execute();
	    		
	    	}


	    	  public function Affichervehicule()
{
             $req = array();
	    	$requete = " SELECT* FROM vehicule";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $obj= new Vehicule ($donnee['idVehicule'],$donnee['plaque'],$donnee['modele'],$donnee['type'],$donnee['numeroSerie'],$donnee['proprietaire']); 
	    	    $obj->setIdVehicule($donnee['idVehicule']);
	    	    $req[]=$obj;
	    	   
	    	}
	    	return $req;    
}

  //supprimer un objet 
public function supprimer($id)
{
$this->_db->exec("DELETE FROM vehicule WHERE idVehicule=".$id);
}//fin supprimer*/


//============================================================pour afficher le vehicule dans controle,suivi,entretien===============================================

	  public function Affichervehiculedonner($id)
{
             $req = array();
	    	$requete = " SELECT* FROM vehicule where vehicule.idVehicule=".$id;
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $obj= new Vehicule ($donnee['idVehicule'],$donnee['plaque'],$donnee['modele'],$donnee['type'],$donnee['numeroSerie'],$donnee['proprietaire']); 
	    	    $obj->setIdVehicule($donnee['idVehicule']);
	    	    $req[]=$obj;
	    	   
	    	}
	    	return $req;    
}


//recherche pour afficher un objet
		 public function chercher($vehicule)
{
             $req = array();
	    	$requete = (" SELECT  *  FROM vehicule where vehicule.idVehicule=".$vehicule);
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$obj= new Vehicule ($donnee['idVehicule'],$donnee['plaque'],$donnee['modele'],$donnee['type'],$donnee['numeroSerie'],$donnee['proprietaire']); 
	    	    $obj->setIdVehicule($donnee['idVehicule']);
	    	    $req[]=$obj;
	    	
	    	}
	    	return $req;  
}


 public function modifier($vehicule) {

	    $req= $this->_db ->prepare("UPDATE vehicule set plaque= :p, modele= :m , type= :t , numeroSerie= :n , proprietaire= :p  
	    	where idVehicule =".$vehicule);

	    	$req-> bindValue(':id',$_POST['plaque']);
	    	$req-> bindValue(':m',$_POST['modele']);
	    	$req-> bindValue(':t',$_POST['type']);
	    	$req-> bindValue(':n',$_POST['numeroSerie']);
	    	$req-> bindValue(':p',$_POST['proprietaire']);
	   
			$req->execute();
		
		}

}
	

 ?>