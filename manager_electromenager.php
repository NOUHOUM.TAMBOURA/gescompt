
<?php
require("class_creation_electromenager.php");
require("Classe_Electromenager.php");
require("Class_conso_electromenager.php");

class Managerelectromenager
{
private $_db; // Instance de PDO
public function __construct($db)
{
$this->setDb($db);
}


public function setDb(PDO $db)
{
$this->_db = $db;
}


//===============================================pour la consommation des compteurs========================================


public function ajouterconso(ConsoElectromenager $ccs){
	       $req = $this->_db->prepare("
	                     INSERT INTO ConsoElectromenager (`id`, `index1`, `datee`, `heure`, `consommation`, `idelectro`) 
	                               VALUES (null,:index,:dr,:hr,:cons,:idc)");
	   
	    	/*$req-> bindValue(':id',$ccs->getIdEdm_a());*/
	    	$req-> bindValue(':index',$ccs->getIndex1());
	    	$req-> bindValue(':dr',$ccs->getDatee());
	    	$req-> bindValue(':hr',$ccs->getHeure());
	    	$req-> bindValue(':cons',$ccs->getConsommation());	
	    	$req-> bindValue(':idc',$ccs->getIdelectro());	 	    	
	    	/*$req-> bindValue(':structure',$user->getStructure());*/
	    	
	    	$req->execute();

	    }


// je recupere de dernier id du compteur
   public function idmax($max)
{
     return $max1=$this->_db->query("SELECT MAX(id)  FROM ConsoElectromenager where idelectro='$max'")->fetchColumn();

}
// je recupere index de ce compteur

   public function maxindex($max1,$nom)
{
     return $valeur=$this->_db->query("SELECT index1  FROM ConsoElectromenager where id=$max1 and idelectro='$nom'")->fetchColumn();
}


 public function Afficher()
{
             $req = array();
	    	$requete = " SELECT* FROM creationelectromenager ";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	   $req[] = new Electromenagers ($donnee['idelectro'],$donnee['lieu'],$donnee['ville'],$donnee['nom']); 
	    	    
	    	}
	    	return $req;    
}


	  public function Afficherconso($code)
{
             $req = array();
	    	$requete = " SELECT* FROM ConsoElectromenager where idelectro='$code'";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $req[] = new ConsoElectromenager($donnee['id'],$donnee['index1'],$donnee['datee'],$donnee['heure'],$donnee['consommation'],$donnee['idelectro']); 
	    	   
	    	}
	    	return $req;    
}



    //Ajouter un compteur
/*	public function ajouter(Electromenager $e){
	       $req = $this->_db->prepare("
	                     INSERT INTO electromenager (`idelectro`, `lieu`, `datee`, `heure`, `indexElectro`) 
	                               VALUES (null,:l,:dt,:hr,:index)");
	   
	    	$req-> bindValue(':l',$e->getLieu());
	    	$req-> bindValue(':dt',$e->getDatee());
	    	$req-> bindValue(':hr',$e->getHeure());
	    	$req-> bindValue(':index',$e->getIndexElectro());	 	    	
	    	
	    	$req->execute();
	    		
	    	}

	  public function Afficher()
{
             $req = array();
	    	$requete = " SELECT* FROM electromenager order by idelectro DESC ";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $obj = new Electromenager ($donnee['lieu'],$donnee['datee'],$donnee['heure'],$donnee['indexElectro']); 
	    	    $obj->setIdelectro($donnee['idelectro']);
	    	    $req[]=$obj;
	    	}
	    	return $req;    
}

   //supprimer un objet 
public function supprimer($e)
{
$this->_db->exec("DELETE FROM electromenager WHERE idelectro=".$e);
}//fin supprimer*/


//modifier un objet avec son id en parametre 
/* public function modifier($e) {

	    $req= $this->_db ->prepare("UPDATE electromenager set lieu= :l, datee= :dt, heure = :hr,indexElectro= :ind where idelectro =".$e);

	    	$req-> bindValue(':l',$_POST['lieu']);
	    	$req-> bindValue(':dt',$_POST['date']);
	    	$req-> bindValue(':hr',$_POST['heure']);
	    	$req-> bindValue(':ind',$_POST['index']);
			$req->execute();
		
		}

		//recherche pour afficher un objet
		 public function chercher($e)
{
             $req = array();
	    	$requete = (" SELECT * FROM electromenager where idelectro=".$e);
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$req[]= new Electromenager ($donnee['lieu'],$donnee['datee'],$donnee['heure'],$donnee['indexElectro']); 
	    	
	    	}
	    	return $req;   
}*/

//================================================pour les Appareil Elecromenager==================================================

	public function ajouterElctro(Electromenagers $e){
	       $req = $this->_db->prepare("
	                     INSERT INTO creationelectromenager (`idelectro`, `lieu`, `ville`, `nom`) 
	                               VALUES (:id,:l,:v,:n)");
	   
	    	$req-> bindValue(':id',$e->getIdelectro());
	    	$req-> bindValue(':l',$e->getLieu());
	    	$req-> bindValue(':v',$e->getVille());
	    	$req-> bindValue(':n',$e->getNom());	 	    	
	    	
	    	$req->execute();
	    		
	    	}


	    		  public function AfficherElecro()
{
             $req = array();
	    	$requete = " SELECT* FROM creationelectromenager ";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	   $req[] = new Electromenagers ($donnee['idelectro'],$donnee['lieu'],$donnee['ville'],$donnee['nom']); 
	    	    
	    	   
	    	}
	    	return $req;    
}


   //supprimer un objet 
public function supprimerElectro($e)
{
$this->_db->exec("DELETE FROM creationelectromenager WHERE idelectro='$e'");
}//fin supprimer*/


		 public function chercherElectro($e)
{
             $req = array();
	    	$requete = (" SELECT * FROM creationelectromenager where idelectro='$e'");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$req[] = new Electromenagers ($donnee['idelectro'],$donnee['lieu'],$donnee['ville'],$donnee['nom']); 
	    	
	    	}
	    	return $req;   
}

public function modifierElectro($e) {

	    $req= $this->_db ->prepare("UPDATE creationelectromenager set idelectro= :l, lieu= :dt, ville = :hr,nom= :ind where idelectro ='$e'");

	    	$req-> bindValue(':l',$_POST['identifiant']);
	    	$req-> bindValue(':dt',$_POST['lieu']);
	    	$req-> bindValue(':hr',$_POST['ville']);
	    	$req-> bindValue(':ind',$_POST['nom']);
			$req->execute();
		
		}

	}

	

?>