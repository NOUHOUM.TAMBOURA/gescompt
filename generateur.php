<?php 

class Generateur

{
	Private $idgenerateur;
	private $nom;
	private $modele;
	private $localisation;
	private $marque;
	private $energie;
	

public function __construct($id,$nom,$modele,$localisation,$marque,$energie){
		$this->idgenerateur =$id;
		$this->nom =$nom;
		$this->modele =$modele;
		$this->localisation =$localisation;
		$this->marque =$marque;
		$this->energie =$energie;
	}


	// Les getters
	public function getIdgenerateur(){return $this->idgenerateur;}
	public function getNom(){return $this->nom;}
	public function getModele(){return $this->modele;}
	public function getLocalisation(){return $this->localisation;}
	public function getMarque(){return $this->marque;}
	public function getEnergie(){return $this->energie;} 
	
 
	// les setteurs
	public function setIdgenerateur($idgenerateur)
	{
		$this->idgenerateur= $idgenerateur;
	}
	public function setNom($nom)
	{
		$this->nom = $nom;
		
	}
	public function setModele($modele)
	{

		$this->modele = $modele;
		
	}
	public function setLocalisation($localisation)
	{

		$this->localisation = $localisation;
	}
	public function setMarque($marque)
	{

		$this->marque = $marque;
	}
	public function setEnergie($energie)
	{

		$this->energie = $energie;
	}
	

}


//========================================== manager generateur ===============================================

class Managergenerateur
{
private $_db; // Instance de PDO
public function __construct($db)
{
$this->setDb($db);
}


public function setDb(PDO $db)
{
$this->_db = $db;
}


//===============================================pour la consommation des compteurs========================================


public function ajoutergenerateur(Generateur $generateur){
	       $req = $this->_db->prepare("
	                     INSERT INTO generateur (`idGenerateur`, `nom`, `modele`, `localisation`, `marque`, `energie`) 
	                               VALUES (null,:index,:dr,:hr,:cons,:idc)");
	   
	    	/*$req-> bindValue(':id',$ccs->getIdEdm_a());*/
	    	$req-> bindValue(':index',$generateur->getNom());
	    	$req-> bindValue(':dr',$generateur->getModele());
	    	$req-> bindValue(':hr',$generateur->getLocalisation());
	    	$req-> bindValue(':cons',$generateur->getMarque());	
	    	$req-> bindValue(':idc',$generateur->getEnergie());	 	    	
	    	/*$req-> bindValue(':structure',$user->getStructure());*/
	    	
	    	$req->execute();

	    }

	     public function Affichergenerateur()
{
             $req = array();
	    	$requete = " SELECT* FROM generateur ";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	   $req[] = new Generateur ($donnee['idGenerateur'],$donnee['nom'],$donnee['modele'],$donnee['localisation'],$donnee['marque'],$donnee['energie']); 
	    	    
	    	}
	    	return $req;    
}

 //supprimer un objet 
public function supprimer($id)
{
$this->_db->exec("DELETE FROM generateur WHERE idGenerateur=".$id);
}//fin supprimer*/

		//recherche pour afficher un objet
		 public function chercher($generateur)
{
             $req = array();
	    	$requete = (" SELECT  *  FROM generateur where generateur.idGenerateur=".$generateur);
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$req[] = new Generateur ($donnee['idGenerateur'],$donnee['nom'],$donnee['modele'],$donnee['localisation'],$donnee['marque'],$donnee['energie']);
	    	
	    	}
	    	return $req;   
}


 public function modifier($generateur) {

	    $req= $this->_db ->prepare("UPDATE generateur set nom= :id, modele= :v, localisation = :l,marque= :n ,energie= :e where idGenerateur =".$generateur);

	    	$req-> bindValue(':id',$_POST['nom']);
	    	$req-> bindValue(':v',$_POST['modele']);
	    	$req-> bindValue(':l',$_POST['localisation']);
	    	$req-> bindValue(':n',$_POST['marque']);
	    	$req-> bindValue(':e',$_POST['energie']);
			$req->execute();
		
		}


}


 ?>