<?php
/*require("class_conso_testeur_edm_n.php");

;*/
require("sommeSurAcceuil.php");
require("class_creation_conso_edm_n.php");
require('managerchercheridentifiant.php');
require('managerconsotesteur_edm_n.php');
include('autorisation.php');
 include('config.php');

 $ud=new ManagerConso_testeur_Edm_n($db);
$r=new Recherche($db);
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="icon" href="img/logo1.png">

  <title>gescompt</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
     <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon rotate-n-25">
          <i class=""></i>
        </div>
        <div class="sidebar-brand-text mx-3"><?php echo strtoupper($prenom ."<br>" .$nom);?> 

          </div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->

        <?php if ($_SESSION['statut']==1) { ?> 
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th>
       <li class="nav-item active">
        <a class="nav-link" href="utilisateur.php">
          <i class=""></i>
         <span class="text-center"> <h2 class="btn btn-danger col-sm-12">Utilisateur</h2></span></a>
      </li>
      <?php }  ?>


      <!-- gestion parc auto -->
        <?php if ($_SESSION['statut']==4 ||  ($_SESSION['statut']==1)) { ?> 
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th>
       <li class="nav-item active">
        <a class="nav-link" href="ajoutervehicule.php">
          <i class=""></i>
         <span class="text-center"> <h2 class="btn btn-info col-sm-12">Parc Auto</h2></span></a>
      </li>
      <?php }  ?>

         <!-- gestion groupe electrogene -->
        <?php if ($_SESSION['statut']==4 ||  ($_SESSION['statut']==1)) { ?> 
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th>
       <li class="nav-item active">
        <a class="nav-link" href="ajoutergenerateur.php">
          <i class=""></i>
         <span class="text-center"> <h2 class="btn btn-warning col-sm-12">Génerateur</h2></span></a>
      </li>
      <?php }  ?>



      <li class="nav-item active">
        <a class="nav-link" href="index.php">
          <i class=""></i>
         <span class="text-center text-uppercase"> <h2 class="btn btn-info col-sm-12">ACCUEIL </h2></span></a>
      </li>


<?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        GESTION DES COMPTEURS
      </div>

      <?php }  ?>

      <!-- Nav Item - Pages pour la creation des compteurs -->
      <?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
                  

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Creation</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
            <a class="collapse-item btn btn-primary" href="creationsomagep.php">Somagep</a><br>
            <a class="collapse-item btn btn-success" href="creation_edm_numerique.php"> EDM Numérique</a><br>
            <a class="collapse-item btn btn-info" href="creation_edm_analogique.php">EDM Analogique</a><br>
            <a class="collapse-item btn btn-warning" href="cration_electromenager.php"> Electromenager</a>
          </div>
        </div>
      </li>
      <?php }  ?>

<?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
       <hr class="sidebar-divider">
      <!-- Nav Item - pour la consommation des compteurs -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Releve / Index</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
            <a class="collapse-item btn btn-primary" href="consommation_somagep.php">Somagep</a><br>
            <a class="collapse-item btn btn-success" href="consommation_edm_numerique.php">EDM Numérique</a><br>
            <a class="collapse-item btn btn-info" href="consommation_edm_analogique.php">EDM Analogique</a><br>
            <a class="collapse-item btn btn-warning" href="consoelectromenager.php">Electromenager</a>
          </div>
        </div>
      </li>
      <?php }  ?>
      <!-- Divider -->


      <?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 

      <hr class="sidebar-divider">
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Statistique / Conso</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
           <!--  <a class="collapse-item" href="statistiques.php">Statistique</a> -->
            <a class="collapse-item btn btn-primary" href="statistiques_somagep.php">Somagep</a><br>
            <a class="collapse-item btn btn-success" href="statistiques_edm_numerique.php">EDM Numérique</a><br>
            <a class="collapse-item btn btn-info" href="statistiques_edm_analogique.php">EDM Analogique</a>
           <!--  <a class="collapse-item" href="statistiques_electromenager.php">Electromenager</a> -->
           
          </div>
        </div>
      </li>
      <?php }  ?>
  

    

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
      <!--     <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Recherche" aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form> -->

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Recherche" aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - Alerts -->
         

            <!-- Nav Item - Messages -->
          

       

          </ul>
<h2 style="margin-right: 30%;" class="text-primary">Système Informatique de Gestion des Compteurs</h2>
              <a href="deconnexion.php"><button type="button" class="btn btn-danger">Deconnexion</button></a>
        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
        <br>

          <!-- Content Row -->
          

          <!-- les contenues -->
<!-- ==================================================================================== -->
<div class="container">

    <!-- Project Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Statistiques</h6>
                </div>
                <div class="card-body">
                  <h4 class="small font-weight-bold">Somagep <span class="float-right">20%</span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h4 class="small font-weight-bold">EDM Numeriques <span class="float-right">40%</span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h4 class="small font-weight-bold">EDM Analogiques <span class="float-right">60%</span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h4 class="small font-weight-bold">Elecromenagers <span class="float-right">0%</span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar bg-info" role="progressbar" style="width:0%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>



                    <form class="user" method="POST" action="">
                 <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                   <span class="text-primary">Date Debut :</span>
                    <input type="date" class="form-control " id="exampleFirstName" placeholder="index du compteur" name="ddebut" required="on">
                  </div>
                  <div class="col-sm-6">
                    <span class="text-primary">Date Fin :</span>
                    <input type="date" class="form-control " id="exampleLastName" placeholder="date releve" name="dfin" required="on">
                  </div>

                </div>
 
                </div>
               

    <div class="row" style="margin-left: 90px;">
            <div class="col-md-12">
              <button type="submit" class="btn btn-success" name="Enregistrer" 
              style="margin-left: 70px;">
            Chercher
              </button>
              
            </div>

        </div>
<?php  
   ?>

              </form>
          

         
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
<!-- Footer -->
   <!--    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; 2019</span>
          </div>
        </div>
      </footer>
 -->
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

</body>

</html>
