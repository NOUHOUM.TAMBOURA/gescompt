<?php 

require('generateur.php');

class Suivigenerateur

{
	Private $idsuivieG;
	private $tensionbatterie;
	private $charge;
	private $volteSortie;
	private $amperageSortie;
	private $frequence;
	private $heurefonctionnement;
	private $niveaucarburant;
	private $nouveauajoutcarburant;
	private $idGenerateur;
	

public function __construct($idsuivieG,$tensionbatterie,$charge,$volteSortie,$amperageSortie,$frequence,
	$heurefonctionnement,$niveaucarburant,$nouveauajoutcarburant,$idGenerateur){
		$this->idsuivieG =$idsuivieG;
		$this->tensionbatterie =$tensionbatterie;
		$this->charge =$charge;
		$this->volteSortie =$volteSortie;
		$this->amperageSortie =$amperageSortie;
		$this->frequence =$frequence;
		$this->heurefonctionnement =$heurefonctionnement;
		$this->niveaucarburant =$niveaucarburant;
		$this->nouveauajoutcarburant =$nouveauajoutcarburant;
		$this->idGenerateur =$idGenerateur;
	}


	// Les getters
	public function getIdsuivieG(){return $this->idsuivieG;}
	public function getTensionbatterie(){return $this->tensionbatterie;}
	public function getCharge(){return $this->charge;}
	public function getVolteSortie(){return $this->volteSortie;}
	public function getAmperageSortie(){return $this->amperageSortie;}
	public function getFrequence(){return $this->frequence;}
	public function getHeurefonctionnement(){return $this->heurefonctionnement;} 
	public function getNiveaucarburant(){return $this->niveaucarburant;} 
	public function getNouveauajoutcarburant(){return $this->nouveauajoutcarburant;} 
	public function getIdGenerateur(){return $this->idGenerateur;} 
	
 
	// les setteurs
	public function setIdsuivieG($idsuivieG)
	{
		$this->idsuivieG= $idsuivieG;
	}
	public function setTensionbatterie($tensionbatterie)
	{
		$this->tensionbatterie = $tensionbatterie;
		
	}
	public function setCharge($charge)
	{
		$this->charge = $charge;
		
	}
	public function setVolteSortie($volteSortie)
	{

		$this->volteSortie = $volteSortie;
		
	}
	public function setAmperageSortie($amperageSortie)
	{

		$this->amperageSortie = $amperageSortie;
	}
	public function setFrequence($frequence)
	{

		$this->frequence = $frequence;
	}
	public function setHeurefonctionnement($heurefonctionnement)
	{

		$this->heurefonctionnement = $heurefonctionnement;
	}
	public function setNiveaucarburant($niveaucarburant)
	{

		$this->niveaucarburant = $niveaucarburant;
	}
	public function setNouveauajoutcarburant($nouveauajoutcarburant)
	{

		$this->nouveauajoutcarburant = $nouveauajoutcarburant;
	}
	public function setIdGenerateur($idGenerateur)
	{

		$this->idGenerateur = $idGenerateur;
	}
	

}


//========================================== manager suivi generateur ===============================================

class Managersuivigenerateur
{
private $_db; // Instance de PDO
public function __construct($db)
{
$this->setDb($db);
}


public function setDb(PDO $db)
{
$this->_db = $db;
}


//===============================================pour la consommation des compteurs========================================


public function ajoutersuivigenerateur(Suivigenerateur $suivigenerateur){
	       $req = $this->_db->prepare("
	                     INSERT INTO suiviegenerateur (`idsuivieG`, `tensionbatterie`, `charge`, `volteSortie`, `amperageSortie`, `frequence`, `heurefonctionnement`, `niveaucarburant`, `nouveauajoutcarburant`, `idGenerateur`) 
	                               VALUES (null,:index,:dr,:hr,:cons,:idc,:h,:n,:nj,:id)");
	   
	    	/*$req-> bindValue(':id',$ccs->getIdEdm_a());*/
	    	$req-> bindValue(':index',$suivigenerateur->getTensionbatterie());
	    	$req-> bindValue(':dr',$suivigenerateur->getVolteSortie());
	    	$req-> bindValue(':hr',$suivigenerateur->getCharge());
	    	$req-> bindValue(':cons',$suivigenerateur->getAmperageSortie());	
	    	$req-> bindValue(':idc',$suivigenerateur->getFrequence());	
	    	$req-> bindValue(':h',$suivigenerateur->getHeurefonctionnement());	
	    	$req-> bindValue(':n',$suivigenerateur->getNiveaucarburant());	
	    	$req-> bindValue(':nj',$suivigenerateur->getNouveauajoutcarburant());	 	    	
	    	$req-> bindValue(':id',$suivigenerateur->getIdGenerateur());
	    	
	    	$req->execute();

	    }

	     public function Affichersuivigenerateur($id)
{
             $req = array();
	    	$requete = " SELECT* FROM suiviegenerateur,generateur where suiviegenerateur.idGenerateur= generateur.idGenerateur  and suiviegenerateur.idGenerateur=".$id;
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	   $req[] = new Suivigenerateur ($donnee['idsuivieG'],$donnee['tensionbatterie'],$donnee['charge'],$donnee['volteSortie'],$donnee['amperageSortie'],$donnee['frequence']
	    	,$donnee['heurefonctionnement'],$donnee['niveaucarburant'],$donnee['nouveauajoutcarburant'],$donnee['idGenerateur']); 
	    	    
	    	}
	    	return $req;    
}



	     public function Affichergenerateur($id)
{
             $req = array();
	    	$requete = " SELECT* FROM generateur where generateur.idGenerateur=".$id;
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	   $req[] = new Generateur ($donnee['idGenerateur'],$donnee['nom'],$donnee['modele'],$donnee['localisation'],$donnee['marque'],$donnee['energie']); 
	    	    
	    	}
	    	return $req;    
}

 //supprimer un objet 
public function supprimer($id)
{
$this->_db->exec("DELETE FROM suiviegenerateur WHERE idsuivieG=".$id);
}//fin supprimer*/


//recherche pour afficher un objet
		 public function chercher($suiviegenerateur)
{
             $req = array();
	    	$requete = (" SELECT  *  FROM suiviegenerateur where suiviegenerateur.idsuivieG=".$suiviegenerateur);
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	 $req[] = new Suivigenerateur ($donnee['idsuivieG'],$donnee['tensionbatterie'],$donnee['charge'],$donnee['volteSortie'],$donnee['amperageSortie'],
	    	 	$donnee['frequence']
	    	,$donnee['heurefonctionnement'],$donnee['niveaucarburant'],$donnee['nouveauajoutcarburant'],$donnee['idGenerateur']); 
	    	
	    	}
	    	return $req;   
}


 public function modifier($suiviegenerateur) {

	    $req= $this->_db ->prepare("UPDATE suiviegenerateur set tensionbatterie= :id, charge= :l, volteSortie = :n,amperageSortie= :e,frequence= :h
	    ,heurefonctionnement= :niv,niveaucarburant= :nou,nouveauajoutcarburant= :v where idsuivieG =".$suiviegenerateur);

	    	$req-> bindValue(':id',$_POST['tension']);
	    	$req-> bindValue(':l',$_POST['charge']);
	    	$req-> bindValue(':n',$_POST['voltage']);
	    	$req-> bindValue(':e',$_POST['amperage']);
	    	$req-> bindValue(':h',$_POST['frequence']);
	    	$req-> bindValue(':niv',$_POST['heure']);
	    	$req-> bindValue(':nou',$_POST['niveau']);
	    	$req-> bindValue(':v',$_POST['nouveau']);
			$req->execute();
		
		}

}


 ?>