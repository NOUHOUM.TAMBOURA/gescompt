<?php 

class ConsoEdmN

{
	Private $idcons;
	private $indexheurecreuse;
	private $indexheurepleine;
	private $indexheurepoint;
	private $dateHeureReleve;
	private $dateReleve;
	private $heureReleve;
	private $consommation;
	private $idedm_n;
	


	public function  __construct($idcons,$indexhc,$indexhp,$indexhpt,$datereleve,$heurereleve,$drv,$consommation,$id){
		$this->idcons =$idcons;
		$this->indexheurecreuse =$indexhc;
		$this->indexheurepleine =$indexhp;
		$this->indexheurepoint =$indexhpt;
		$this->dateReleve =$datereleve;
		$this->heureReleve =$heurereleve;
		$this->dateHeureReleve =$drv;
		$this->consommation =$consommation;
		$this->idedm_n =$id;
	}


	// Les getters
	public function getIdcons(){return $this->idcons;}
	public function getIndexheurecreuse(){return $this->indexheurecreuse;}
	public function getIndexheurepleine(){return $this->indexheurepleine;}
	public function getIndexheurepoint(){return $this->indexheurepoint;} 
	public function getDateHeureReleve(){return $this->dateHeureReleve;} 
	public function getDateReleve(){return $this->dateReleve;} 
	public function getHeureReleve(){return $this->heureReleve;} 
	public function getConsommation(){return $this->consommation;}	
	public function getIdedm_n(){return $this->idedm_n;} 
 
	// les setteurs
	public function setIdcons($idcons)
	{
		$idcons = (int)$idcons;
		$this->idcons = $idcons;
	}
	

	public function setIndexheurecreuse($indexheurecreuse)
	{
		$this->indexheurecreuse = $indexheurecreuse;
	}

	public function setIndexheurepleine($indexheurepleine)
	{
		$this->indexheurepleine = $indexheurepleine;
	}

	public function setIndexheurepoint($indexheurepoint)
	{
		$this->indexheurepoint = $indexheurepoint;
	}

	
	public function setDateHeureReleve($dateHeureReleve)
	{
		$this->dateHeureReleve = $dateHeureReleve;
	
	}

	public function setHeureReleve($dateReleve)
	{
		$this->dateReleve = $dateReleve;
	
	}


	public function setConsommation($consommation)
	{
		$this->consommation = $consommation;
	
	}

	public function setIdedm_n($idedm_n)
	{
		$this->idedm_n = $idedm_n;
	
	}
	
}
	


 ?>