-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 03 jan. 2020 à 15:10
-- Version du serveur :  10.1.25-MariaDB
-- Version de PHP :  7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gescompt`
--

-- --------------------------------------------------------

--
-- Structure de la table `comptedm_a`
--

CREATE TABLE `comptedm_a` (
  `idedm_a` varchar(50) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `localisation` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `comptedm_a`
--

INSERT INTO `comptedm_a` (`idedm_a`, `ville`, `localisation`, `nom`) VALUES
('47867843', 'SEVE', 'Bamako', 'EDM_A'),
('84657844', 'LunaPark', 'Bamako', 'EDM_A'),
('854568347', 'Extention', 'Bamako', 'EDM_A'),
('comptKayes107328', 'KayesVille', 'Kayes', 'EDM_A'),
('comptSegou', 'SegouNord', 'Segou', 'EDM_A');

-- --------------------------------------------------------

--
-- Structure de la table `comptedm_n`
--

CREATE TABLE `comptedm_n` (
  `idedm_n` varchar(50) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `localisation` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `comptedm_n`
--

INSERT INTO `comptedm_n` (`idedm_n`, `ville`, `localisation`, `nom`) VALUES
('53093921', 'Badalabougou', 'Bamako', 'EDM_N'),
('73002558', 'Siege', 'Bamako', 'EDM_N'),
('73030891', 'Nouveau CE', 'Bamako', 'EDM_N');

-- --------------------------------------------------------

--
-- Structure de la table `comptsomagep`
--

CREATE TABLE `comptsomagep` (
  `idcomptS` varchar(50) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `localisation` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `comptsomagep`
--

INSERT INTO `comptsomagep` (`idcomptS`, `ville`, `localisation`, `nom`) VALUES
('A10S683325', 'Compteur principal immeuble', 'Bamako', 'SOMAGEP'),
('A13S559538', 'Ancien domicine DG', 'Bamako', 'SOMAGEP'),
('Compt2598', 'Extention', 'Bamako', 'SOMAGEP'),
('SIEGE', 'Bamako', 'SOMAGEP', 'A13S559536');

-- --------------------------------------------------------

--
-- Structure de la table `concomptsomagep`
--

CREATE TABLE `concomptsomagep` (
  `idcons` int(11) NOT NULL,
  `index1` varchar(50) DEFAULT NULL,
  `datereleve` date NOT NULL,
  `heurereleve` time NOT NULL,
  `dateheurerevele` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `consommation` varchar(50) NOT NULL,
  `idcomptS` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `concomptsomagep`
--

INSERT INTO `concomptsomagep` (`idcons`, `index1`, `datereleve`, `heurereleve`, `dateheurerevele`, `consommation`, `idcomptS`) VALUES
(33, '100', '2019-12-18', '11:50:00', '2019-12-18 11:54:31', '100', 'A10S683325'),
(34, '100', '2019-12-18', '11:50:00', '2019-12-18 11:54:35', '100', 'SIEGE'),
(35, '100', '2019-12-18', '11:50:00', '2019-12-18 11:54:39', '100', 'A13S559538'),
(36, '100', '2019-12-18', '11:50:00', '2019-12-18 11:54:42', '100', 'Compt2598'),
(40, '200', '2019-12-21', '09:10:00', '2019-12-18 12:06:31', '50', 'A10S683325'),
(41, '300', '2019-12-21', '10:10:00', '2019-12-18 23:45:19', '200', 'A10S683325'),
(42, '400', '2019-12-19', '02:02:00', '2019-12-18 23:46:41', '100', 'SIEGE'),
(43, '500', '2020-01-01', '02:02:00', '2019-12-18 23:47:34', '200', 'A10S683325'),
(44, '1000', '2020-01-02', '10:09:00', '2019-12-18 23:48:30', '500', 'A10S683325'),
(45, '200', '2019-12-22', '02:01:00', '2019-12-22 21:01:09', '100', 'Compt2598'),
(46, '300', '2019-12-23', '10:06:00', '2019-12-22 21:02:02', '100', 'Compt2598'),
(47, '500', '2019-12-24', '04:07:00', '2019-12-22 21:03:37', '200', 'Compt2598'),
(48, '560', '2019-12-25', '02:02:00', '2019-12-22 21:05:29', '60', 'Compt2598');

-- --------------------------------------------------------

--
-- Structure de la table `consedm_a`
--

CREATE TABLE `consedm_a` (
  `idcons` int(11) NOT NULL,
  `index1` varchar(50) DEFAULT NULL,
  `datereleve` date NOT NULL,
  `heurereleve` time NOT NULL,
  `dateheurereleve` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `consommation` varchar(50) NOT NULL,
  `idedm_a` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `consedm_a`
--

INSERT INTO `consedm_a` (`idcons`, `index1`, `datereleve`, `heurereleve`, `dateheurereleve`, `consommation`, `idedm_a`) VALUES
(1, '100', '2019-12-18', '12:11:00', '2019-12-18 12:11:23', '100', '47867843'),
(2, '100', '2019-12-18', '12:15:00', '2019-12-18 12:15:53', '100', '84657844'),
(4, '100', '2019-12-18', '12:15:00', '2019-12-18 12:17:01', '100', '854568347'),
(5, '100', '2019-12-18', '12:15:00', '2019-12-18 12:17:01', '100', 'comptKayes107328'),
(6, '100', '2019-12-18', '12:15:00', '2019-12-18 12:17:01', '100', 'comptSegou'),
(7, '110', '2019-12-19', '10:10:00', '2019-12-18 12:23:05', '10', '47867843'),
(8, '130', '2019-12-20', '10:20:00', '2019-12-18 12:23:38', '20', '47867843'),
(9, '150', '2019-12-21', '10:11:00', '2019-12-18 12:24:41', '20', '47867843'),
(10, '120', '2019-12-19', '10:10:00', '2019-12-18 12:25:30', '20', '84657844'),
(12, '110', '2019-12-19', '10:00:00', '2019-12-18 12:28:54', '10', 'comptKayes107328'),
(13, '150', '2019-12-20', '11:00:00', '2019-12-18 12:29:25', '40', 'comptKayes107328'),
(14, '200', '2019-12-21', '10:10:00', '2019-12-18 12:29:50', '50', 'comptKayes107328'),
(16, '250', '2019-12-22', '01:02:00', '2019-12-18 12:32:47', '50', 'comptKayes107328'),
(17, '300', '2019-12-23', '10:01:00', '2019-12-18 12:33:13', '50', 'comptKayes107328'),
(18, '100', '2019-12-18', '12:33:00', '2019-12-18 12:34:17', '100', 'comptSegou'),
(19, '150', '2019-12-19', '10:10:00', '2019-12-18 12:36:17', '50', 'comptSegou'),
(20, '200', '2019-12-20', '11:00:00', '2019-12-18 12:36:43', '50', 'comptSegou'),
(21, '300', '2019-12-21', '11:00:00', '2019-12-18 12:37:17', '100', 'comptSegou');

-- --------------------------------------------------------

--
-- Structure de la table `consedm_n`
--

CREATE TABLE `consedm_n` (
  `idcons` int(11) NOT NULL,
  `indexheurecreuse` varchar(50) NOT NULL,
  `indexheurepleine` varchar(50) NOT NULL,
  `indexheurepoint` varchar(50) NOT NULL,
  `datereleve` date NOT NULL,
  `heurereleve` time NOT NULL,
  `dateheurereleve` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `consommation` varchar(50) NOT NULL,
  `idedm_n` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `consedm_n`
--

INSERT INTO `consedm_n` (`idcons`, `indexheurecreuse`, `indexheurepleine`, `indexheurepoint`, `datereleve`, `heurereleve`, `dateheurereleve`, `consommation`, `idedm_n`) VALUES
(104, '100', '100', '100', '2019-12-18', '12:01:00', '2019-12-18 12:01:13', '300', '53093921'),
(105, '110', '110', '110', '2019-12-18', '12:02:00', '2019-12-18 12:01:57', '330', '73002558'),
(106, '120', '120', '120', '2019-12-18', '12:03:00', '2019-12-18 12:02:35', '360', '73030891'),
(107, '120', '120', '120', '2019-12-20', '09:10:00', '2019-12-18 12:38:09', '360', '53093921'),
(108, '1000', '100', '120', '2020-01-01', '02:01:00', '2019-12-19 12:07:09', '1220', '53093921');

-- --------------------------------------------------------

--
-- Structure de la table `consoelectromenager`
--

CREATE TABLE `consoelectromenager` (
  `id` int(11) NOT NULL,
  `index1` varchar(50) NOT NULL,
  `datee` date NOT NULL,
  `heure` time NOT NULL,
  `consommation` varchar(50) NOT NULL,
  `idelectro` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `consoelectromenager`
--

INSERT INTO `consoelectromenager` (`id`, `index1`, `datee`, `heure`, `consommation`, `idelectro`) VALUES
(1, '0', '2019-12-21', '11:26:00', '0', 'Appareil001'),
(2, '0', '2019-12-21', '10:23:00', '0', 'Appareil002'),
(3, '0', '2019-12-21', '09:26:00', '0', 'Appareil003'),
(4, '0', '2019-12-21', '10:25:00', '0', 'Appareil004'),
(5, '0', '2019-12-21', '13:22:00', '0', 'Appareil005'),
(6, '200', '2019-12-22', '01:02:00', '200', 'Appareil001'),
(7, '300', '2019-12-23', '00:00:00', '300', 'Appareil002'),
(8, '200', '2019-12-21', '01:01:00', '200', 'Appareil005'),
(9, '300', '2019-12-21', '01:01:00', '100', 'Appareil005'),
(10, '110', '2019-12-22', '01:02:00', '110', 'Appareil004');

-- --------------------------------------------------------

--
-- Structure de la table `conssouscomptsomagep`
--

CREATE TABLE `conssouscomptsomagep` (
  `idconssous` int(11) NOT NULL,
  `index3` double NOT NULL,
  `datereleve` date NOT NULL,
  `heurereleve` time NOT NULL,
  `dateheurereleve` datetime NOT NULL,
  `consommation` double NOT NULL,
  `idsouscompt` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `constesteuredm_n`
--

CREATE TABLE `constesteuredm_n` (
  `idconstesteur` int(11) NOT NULL,
  `indexheurecreuse` double DEFAULT NULL,
  `indexheurepleine` double DEFAULT NULL,
  `indexheurepointe` double DEFAULT NULL,
  `heurereleve` time DEFAULT NULL,
  `dateheurereleve` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `consommation` varchar(50) DEFAULT NULL,
  `idtesteur` varchar(50) DEFAULT NULL,
  `idcons` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `constesteuredm_n`
--

INSERT INTO `constesteuredm_n` (`idconstesteur`, `indexheurecreuse`, `indexheurepleine`, `indexheurepointe`, `heurereleve`, `dateheurereleve`, `consommation`, `idtesteur`, `idcons`) VALUES
(87, 100, 100, 100, '12:01:00', '2019-12-18 12:01:13', '300', '53093921', 104),
(88, 110, 110, 110, '12:02:00', '2019-12-18 12:01:57', '330', '73002558', 105),
(89, 120, 120, 120, '12:03:00', '2019-12-18 12:02:35', '360', '73030891', 106),
(90, 120, 120, 110, '09:10:00', '2019-12-18 12:38:09', '350', '53093921', 107),
(91, 120, 100, 100, '02:01:00', '2019-12-19 12:07:09', '320', '53093921', 108);

-- --------------------------------------------------------

--
-- Structure de la table `constesteursomagep`
--

CREATE TABLE `constesteursomagep` (
  `idconstesteur` int(11) NOT NULL,
  `datereleve` date NOT NULL,
  `heurereleve` time NOT NULL,
  `dateheurereleve` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `consommation` varchar(50) NOT NULL,
  `idtesteur` varchar(50) NOT NULL,
  `idcons` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `constesteur_a`
--

CREATE TABLE `constesteur_a` (
  `idcons1` int(11) NOT NULL,
  `index1` varchar(50) DEFAULT NULL,
  `datereleve` date NOT NULL,
  `heurereleve` time NOT NULL,
  `dateheurereleve` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `consommation` varchar(50) NOT NULL,
  `idtesteur` varchar(50) NOT NULL,
  `idcons` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `constesteur_a`
--

INSERT INTO `constesteur_a` (`idcons1`, `index1`, `datereleve`, `heurereleve`, `dateheurereleve`, `consommation`, `idtesteur`, `idcons`) VALUES
(38, '100', '2019-12-18', '12:11:00', '2019-12-18 12:14:14', '100', '47867843', 1),
(39, '100', '2019-12-18', '12:18:00', '2019-12-18 12:18:19', '100', '84657844', 2),
(40, '100', '2019-12-18', '12:18:00', '2019-12-18 12:20:02', '100', '854568347', 3),
(41, '100', '2019-12-18', '12:18:00', '2019-12-18 12:20:02', '100', 'comptKayes107328', 4),
(42, '100', '2019-12-18', '12:18:00', '2019-12-18 12:20:02', '100', 'comptSegou', 5),
(43, '120', '2019-12-19', '10:10:00', '2019-12-18 12:23:05', '20', '47867843', 7),
(44, '130', '2019-12-20', '10:20:00', '2019-12-18 12:23:39', '10', '47867843', 8),
(45, '150', '2019-12-21', '10:11:00', '2019-12-18 12:24:41', '20', '47867843', 9),
(46, '130', '2019-12-19', '10:10:00', '2019-12-18 12:25:30', '30', '84657844', 10),
(48, '110', '2019-12-19', '10:00:00', '2019-12-18 12:28:54', '10', 'comptKayes107328', 12),
(49, '150', '2019-12-20', '11:00:00', '2019-12-18 12:29:25', '40', 'comptKayes107328', 13),
(50, '200', '2019-12-21', '10:10:00', '2019-12-18 12:29:50', '50', 'comptKayes107328', 14),
(52, '250', '2019-12-22', '01:02:00', '2019-12-18 12:32:47', '50', 'comptKayes107328', 16),
(53, '300', '2019-12-23', '10:01:00', '2019-12-18 12:33:13', '50', 'comptKayes107328', 17),
(54, '100', '2019-12-18', '12:35:00', '2019-12-18 12:35:23', '100', 'comptSegou', 18),
(55, '150', '2019-12-19', '10:10:00', '2019-12-18 12:36:17', '50', 'comptSegou', 19),
(56, '200', '2019-12-20', '11:00:00', '2019-12-18 12:36:43', '50', 'comptSegou', 20),
(57, '300', '2019-12-21', '11:00:00', '2019-12-18 12:37:17', '100', 'comptSegou', 21);

-- --------------------------------------------------------

--
-- Structure de la table `controle`
--

CREATE TABLE `controle` (
  `idControle` int(11) NOT NULL,
  `typec` varchar(50) DEFAULT NULL,
  `dateD` date DEFAULT NULL,
  `dateF` date DEFAULT NULL,
  `idVehicule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `controle`
--

INSERT INTO `controle` (`idControle`, `typec`, `dateD`, `dateF`, `idVehicule`) VALUES
(1, 'Asurance', '2019-12-24', '2019-12-26', 2),
(2, 'Visite Technique', '2019-12-24', '2019-12-25', 2),
(3, 'Visite Technique', '2020-01-03', '2020-01-04', 2);

-- --------------------------------------------------------

--
-- Structure de la table `creationelectromenager`
--

CREATE TABLE `creationelectromenager` (
  `idelectro` varchar(50) NOT NULL,
  `lieu` varchar(50) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `creationelectromenager`
--

INSERT INTO `creationelectromenager` (`idelectro`, `lieu`, `ville`, `nom`) VALUES
('Appareil001', 'Badalabougou', 'Bamako', 'Cuisine Externe'),
('Appareil002', 'Badalabougou', 'Bamako', 'Frigo vin1'),
('Appareil003', 'Badalabougou', 'Bamako', 'Frigo vin2'),
('Appareil004', 'Badalabougou', 'Bamako', 'Congelateur 1'),
('Appareil005', 'Badalabougou', 'Bamako', 'Congelateur 2');

-- --------------------------------------------------------

--
-- Structure de la table `electromenager`
--

CREATE TABLE `electromenager` (
  `idelectro` int(11) NOT NULL,
  `lieu` varchar(50) NOT NULL,
  `datee` date NOT NULL,
  `heure` time NOT NULL,
  `indexElectro` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `entretien`
--

CREATE TABLE `entretien` (
  `identretien` int(11) NOT NULL,
  `nombreEntretien` varchar(50) NOT NULL,
  `dateE` date NOT NULL,
  `montant` varchar(50) NOT NULL,
  `idVehicule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `entretien`
--

INSERT INTO `entretien` (`identretien`, `nombreEntretien`, `dateE`, `montant`, `idVehicule`) VALUES
(1, 'vidange', '2019-12-24', '200 000', 2),
(5, 'RÃ©paration', '2019-12-25', '11 000', 3),
(8, 'Vidange', '2020-01-01', '100 000', 2),
(9, 'Vidange', '2019-12-26', '100 000', 5),
(10, 'Reparation', '2019-12-27', '100 000', 5),
(11, 'Vidange', '2019-12-27', '100 000', 2),
(12, 'Reparation', '2019-12-27', '11 000', 2),
(13, 'Reparation', '2020-01-01', '220 000', 2),
(14, 'Reparation', '2020-01-03', '100 000', 2);

-- --------------------------------------------------------

--
-- Structure de la table `generateur`
--

CREATE TABLE `generateur` (
  `idGenerateur` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `modele` varchar(50) DEFAULT NULL,
  `localisation` varchar(50) DEFAULT NULL,
  `marque` varchar(50) DEFAULT NULL,
  `energie` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `generateur`
--

INSERT INTO `generateur` (`idGenerateur`, `nom`, `modele`, `localisation`, `marque`, `energie`) VALUES
(2, 'SOMAGEP', 'KIO', 'badalabougou', 'dell', '12344'),
(4, 'Compt001', 'TOYOTA', 'badalabougou', 'HP', '12344'),
(5, 'G001', 'VERSO', 'badalabougou', 'dell', '12344');

-- --------------------------------------------------------

--
-- Structure de la table `souscomptsomagep`
--

CREATE TABLE `souscomptsomagep` (
  `idsouscompt` varchar(50) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `localisation` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `idcomptS` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `suivie`
--

CREATE TABLE `suivie` (
  `idSuivie` int(11) NOT NULL,
  `kilometrage` varchar(50) NOT NULL,
  `dateSv` date NOT NULL,
  `idVehicule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `suivie`
--

INSERT INTO `suivie` (`idSuivie`, `kilometrage`, `dateSv`, `idVehicule`) VALUES
(1, '50 000', '2019-12-24', 2),
(3, '100 000', '2019-12-25', 2),
(4, '40 000', '2020-01-01', 2),
(5, '30 000', '2020-01-02', 5),
(6, '100 000', '2020-01-03', 2);

-- --------------------------------------------------------

--
-- Structure de la table `suiviegenerateur`
--

CREATE TABLE `suiviegenerateur` (
  `idsuivieG` int(11) NOT NULL,
  `tensionbatterie` varchar(50) DEFAULT NULL,
  `charge` varchar(50) DEFAULT NULL,
  `volteSortie` varchar(50) DEFAULT NULL,
  `amperageSortie` varchar(50) DEFAULT NULL,
  `frequence` varchar(50) DEFAULT NULL,
  `heurefonctionnement` varchar(50) DEFAULT NULL,
  `niveaucarburant` varchar(50) DEFAULT NULL,
  `nouveauajoutcarburant` varchar(50) DEFAULT NULL,
  `idGenerateur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `suiviegenerateur`
--

INSERT INTO `suiviegenerateur` (`idsuivieG`, `tensionbatterie`, `charge`, `volteSortie`, `amperageSortie`, `frequence`, `heurefonctionnement`, `niveaucarburant`, `nouveauajoutcarburant`, `idGenerateur`) VALUES
(1, '1233', '234', '3223', '2323', '2343', '323', '232', '2324', 4),
(6, '0001', '0001', '0394', '943', '09435', '02:01', '934', '2000', 2),
(7, '0001', '049', '0394', '049', '09435', '02:02', '934', '2000', 5),
(8, '939', '049', '0001', '049', '09435', '13:11', '934', '2000', 5),
(9, '0001', '3490', '0394', '943', '9034', '01:03', '934', '934', 4),
(10, '0001', '3490', '0001', '943', '9034', '05:06', '934', '934', 2),
(11, '939', '049', '0001', '049', '9034', '06:06', '934', '2000', 2);

-- --------------------------------------------------------

--
-- Structure de la table `testeurcomptsomagep`
--

CREATE TABLE `testeurcomptsomagep` (
  `idtesteur` varchar(50) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `localisation` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `idcomptS` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `testeurcomptsomagep`
--

INSERT INTO `testeurcomptsomagep` (`idtesteur`, `ville`, `localisation`, `nom`, `idcomptS`) VALUES
('A10S683325', 'Compteur principal immeuble', 'Bamako', 'SOMAGEP', 'A10S683325'),
('A13S559536', 'SIEGE', 'Bamako', 'SOMAGEP', 'SIEGE'),
('A13S559538', 'Ancien domicine DG', 'Bamako', 'SOMAGEP', 'A13S559538');

-- --------------------------------------------------------

--
-- Structure de la table `testeuredm_a`
--

CREATE TABLE `testeuredm_a` (
  `idtesteur` varchar(50) NOT NULL,
  `lieu` varchar(50) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `idedm_a` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `testeuredm_a`
--

INSERT INTO `testeuredm_a` (`idtesteur`, `lieu`, `ville`, `nom`, `idedm_a`) VALUES
('47867843', 'SEVE', 'Bamako', 'EDM_A', '47867843'),
('84657844', 'LunaPark', 'Bamako', 'EDM_A', '84657844'),
('854568347', 'Extention', 'Bamako', 'EDM_A', '854568347'),
('comptKayes107328', 'KayesVille', 'Kayes', 'EDM_A', 'comptKayes107328'),
('comptSegou', 'SegouNord', 'Segou', 'EDM_A', 'comptSegou');

-- --------------------------------------------------------

--
-- Structure de la table `testeuredm_n`
--

CREATE TABLE `testeuredm_n` (
  `idtesteur` varchar(50) NOT NULL,
  `lieu` varchar(50) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `idedm_n` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `testeuredm_n`
--

INSERT INTO `testeuredm_n` (`idtesteur`, `lieu`, `ville`, `nom`, `idedm_n`) VALUES
('53093921', 'Badalabougou', 'Bamako', 'EDM_N', '53093921'),
('73002558', 'Siege', 'Bamako', 'EDM_N', '73002558'),
('73030891', 'Nouveau CE', 'Bamako', 'EDM_N', '73030891');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `iduser` int(11) NOT NULL,
  `nomuser` varchar(50) NOT NULL,
  `prenomuser` varchar(50) NOT NULL,
  `emailuser` varchar(50) NOT NULL,
  `identifiantuser` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `statut` varchar(10) NOT NULL,
  `telephone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`iduser`, `nomuser`, `prenomuser`, `emailuser`, `identifiantuser`, `password`, `statut`, `telephone`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', 'admin', 'admin', '1', '56 76 89 80'),
(2, 'admin1', 'admin1', 'admin1@gmail.com', 'admin1', 'admin1', '2', '23 54 66 89'),
(3, 'admin2', 'admin2', 'admin2@gmail.com', 'admin2', 'admin2', '3', '23 43 55 68'),
(4, 'Amadou', 'DIARRA', 'amadou@gmail.com', 'amadou', '12345', '4', '28 99 76 55');

-- --------------------------------------------------------

--
-- Structure de la table `vehicule`
--

CREATE TABLE `vehicule` (
  `idVehicule` int(11) NOT NULL,
  `plaque` varchar(50) NOT NULL,
  `modele` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `numeroSerie` varchar(50) NOT NULL,
  `proprietaire` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `vehicule`
--

INSERT INTO `vehicule` (`idVehicule`, `plaque`, `modele`, `type`, `numeroSerie`, `proprietaire`) VALUES
(2, 'BC-0945-MA', 'TOYOTA', 'V7', 'V002', 'Nouhoum TAMBOURA'),
(3, 'BC-0945-MC', 'TOYOTA', 'V9', 'V003', 'Nouhoum TAMBOURA'),
(5, 'AA-0987-MC', 'VERSO', '4X4', 'S002', 'Mamadou DIARRA');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comptedm_a`
--
ALTER TABLE `comptedm_a`
  ADD PRIMARY KEY (`idedm_a`);

--
-- Index pour la table `comptedm_n`
--
ALTER TABLE `comptedm_n`
  ADD PRIMARY KEY (`idedm_n`);

--
-- Index pour la table `comptsomagep`
--
ALTER TABLE `comptsomagep`
  ADD PRIMARY KEY (`idcomptS`);

--
-- Index pour la table `concomptsomagep`
--
ALTER TABLE `concomptsomagep`
  ADD PRIMARY KEY (`idcons`),
  ADD KEY `ConComptSOMAGEP_ComptSOMAGEP_FK` (`idcomptS`);

--
-- Index pour la table `consedm_a`
--
ALTER TABLE `consedm_a`
  ADD PRIMARY KEY (`idcons`),
  ADD KEY `ConsEDM_A_ComptEDM_A_FK` (`idedm_a`);

--
-- Index pour la table `consedm_n`
--
ALTER TABLE `consedm_n`
  ADD PRIMARY KEY (`idcons`),
  ADD KEY `ConsEDM_N_ComptEDM_N_FK` (`idedm_n`);

--
-- Index pour la table `consoelectromenager`
--
ALTER TABLE `consoelectromenager`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ConCompt_Compt_FK` (`idelectro`);

--
-- Index pour la table `conssouscomptsomagep`
--
ALTER TABLE `conssouscomptsomagep`
  ADD PRIMARY KEY (`idconssous`),
  ADD KEY `ConsSousComptSOMAGEP_SousComptSOMAGEP_FK` (`idsouscompt`);

--
-- Index pour la table `constesteuredm_n`
--
ALTER TABLE `constesteuredm_n`
  ADD PRIMARY KEY (`idconstesteur`),
  ADD KEY `consTesteurEDM_N_TesteurEDM_N_FK` (`idtesteur`),
  ADD KEY `idcons` (`idcons`);

--
-- Index pour la table `constesteursomagep`
--
ALTER TABLE `constesteursomagep`
  ADD PRIMARY KEY (`idconstesteur`),
  ADD KEY `ConsTesteurSOMAGEP_TesteurComptSOMAGEP_FK` (`idtesteur`),
  ADD KEY `idcons` (`idcons`);

--
-- Index pour la table `constesteur_a`
--
ALTER TABLE `constesteur_a`
  ADD PRIMARY KEY (`idcons1`),
  ADD KEY `ConsTesteur_A_TesteurEDM_A_FK` (`idtesteur`),
  ADD KEY `idcons` (`idcons`);

--
-- Index pour la table `controle`
--
ALTER TABLE `controle`
  ADD PRIMARY KEY (`idControle`),
  ADD KEY `controle_controle_FK` (`idVehicule`);

--
-- Index pour la table `creationelectromenager`
--
ALTER TABLE `creationelectromenager`
  ADD PRIMARY KEY (`idelectro`);

--
-- Index pour la table `electromenager`
--
ALTER TABLE `electromenager`
  ADD PRIMARY KEY (`idelectro`);

--
-- Index pour la table `entretien`
--
ALTER TABLE `entretien`
  ADD PRIMARY KEY (`identretien`),
  ADD KEY `entretien_entretien_FK` (`idVehicule`);

--
-- Index pour la table `generateur`
--
ALTER TABLE `generateur`
  ADD PRIMARY KEY (`idGenerateur`);

--
-- Index pour la table `souscomptsomagep`
--
ALTER TABLE `souscomptsomagep`
  ADD PRIMARY KEY (`idsouscompt`),
  ADD KEY `SousComptSOMAGEP_ComptSOMAGEP_FK` (`idcomptS`);

--
-- Index pour la table `suivie`
--
ALTER TABLE `suivie`
  ADD PRIMARY KEY (`idSuivie`),
  ADD KEY `suivie_suivie_FK` (`idVehicule`);

--
-- Index pour la table `suiviegenerateur`
--
ALTER TABLE `suiviegenerateur`
  ADD PRIMARY KEY (`idsuivieG`),
  ADD KEY `suiviegenerateur_suiviegenerateur_FK` (`idGenerateur`);

--
-- Index pour la table `testeurcomptsomagep`
--
ALTER TABLE `testeurcomptsomagep`
  ADD PRIMARY KEY (`idtesteur`),
  ADD KEY `TesteurComptSOMAGEP_ComptSOMAGEP_FK` (`idcomptS`);

--
-- Index pour la table `testeuredm_a`
--
ALTER TABLE `testeuredm_a`
  ADD PRIMARY KEY (`idtesteur`),
  ADD KEY `TesteurEDM_A_ComptEDM_A_FK` (`idedm_a`);

--
-- Index pour la table `testeuredm_n`
--
ALTER TABLE `testeuredm_n`
  ADD PRIMARY KEY (`idtesteur`),
  ADD KEY `TesteurEDM_N_ComptEDM_N_FK` (`idedm_n`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iduser`);

--
-- Index pour la table `vehicule`
--
ALTER TABLE `vehicule`
  ADD PRIMARY KEY (`idVehicule`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `concomptsomagep`
--
ALTER TABLE `concomptsomagep`
  MODIFY `idcons` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT pour la table `consedm_a`
--
ALTER TABLE `consedm_a`
  MODIFY `idcons` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `consedm_n`
--
ALTER TABLE `consedm_n`
  MODIFY `idcons` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT pour la table `consoelectromenager`
--
ALTER TABLE `consoelectromenager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `conssouscomptsomagep`
--
ALTER TABLE `conssouscomptsomagep`
  MODIFY `idconssous` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `constesteuredm_n`
--
ALTER TABLE `constesteuredm_n`
  MODIFY `idconstesteur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT pour la table `constesteursomagep`
--
ALTER TABLE `constesteursomagep`
  MODIFY `idconstesteur` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `constesteur_a`
--
ALTER TABLE `constesteur_a`
  MODIFY `idcons1` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT pour la table `controle`
--
ALTER TABLE `controle`
  MODIFY `idControle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `electromenager`
--
ALTER TABLE `electromenager`
  MODIFY `idelectro` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `entretien`
--
ALTER TABLE `entretien`
  MODIFY `identretien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `generateur`
--
ALTER TABLE `generateur`
  MODIFY `idGenerateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `suivie`
--
ALTER TABLE `suivie`
  MODIFY `idSuivie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `suiviegenerateur`
--
ALTER TABLE `suiviegenerateur`
  MODIFY `idsuivieG` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `vehicule`
--
ALTER TABLE `vehicule`
  MODIFY `idVehicule` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `concomptsomagep`
--
ALTER TABLE `concomptsomagep`
  ADD CONSTRAINT `ConComptSOMAGEP_ComptSOMAGEP_FK` FOREIGN KEY (`idcomptS`) REFERENCES `comptsomagep` (`idcomptS`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `consedm_a`
--
ALTER TABLE `consedm_a`
  ADD CONSTRAINT `ConsEDM_A_ComptEDM_A_FK` FOREIGN KEY (`idedm_a`) REFERENCES `comptedm_a` (`idedm_a`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `consedm_n`
--
ALTER TABLE `consedm_n`
  ADD CONSTRAINT `ConsEDM_N_ComptEDM_N_FK` FOREIGN KEY (`idedm_n`) REFERENCES `comptedm_n` (`idedm_n`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `consoelectromenager`
--
ALTER TABLE `consoelectromenager`
  ADD CONSTRAINT `ConCompt_Compt_FK` FOREIGN KEY (`idelectro`) REFERENCES `creationelectromenager` (`idelectro`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `conssouscomptsomagep`
--
ALTER TABLE `conssouscomptsomagep`
  ADD CONSTRAINT `ConsSousComptSOMAGEP_SousComptSOMAGEP_FK` FOREIGN KEY (`idsouscompt`) REFERENCES `souscomptsomagep` (`idsouscompt`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `controle`
--
ALTER TABLE `controle`
  ADD CONSTRAINT `controle_controle_FK` FOREIGN KEY (`idVehicule`) REFERENCES `vehicule` (`idVehicule`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `entretien`
--
ALTER TABLE `entretien`
  ADD CONSTRAINT `entretien_entretien_FK` FOREIGN KEY (`idVehicule`) REFERENCES `vehicule` (`idVehicule`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `souscomptsomagep`
--
ALTER TABLE `souscomptsomagep`
  ADD CONSTRAINT `SousComptSOMAGEP_ComptSOMAGEP_FK` FOREIGN KEY (`idcomptS`) REFERENCES `comptsomagep` (`idcomptS`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `suivie`
--
ALTER TABLE `suivie`
  ADD CONSTRAINT `suivie_suivie_FK` FOREIGN KEY (`idVehicule`) REFERENCES `vehicule` (`idVehicule`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `suiviegenerateur`
--
ALTER TABLE `suiviegenerateur`
  ADD CONSTRAINT `suiviegenerateur_suiviegenerateur_FK` FOREIGN KEY (`idGenerateur`) REFERENCES `generateur` (`idGenerateur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `testeurcomptsomagep`
--
ALTER TABLE `testeurcomptsomagep`
  ADD CONSTRAINT `TesteurComptSOMAGEP_ComptSOMAGEP_FK` FOREIGN KEY (`idcomptS`) REFERENCES `comptsomagep` (`idcomptS`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `testeuredm_a`
--
ALTER TABLE `testeuredm_a`
  ADD CONSTRAINT `TesteurEDM_A_ComptEDM_A_FK` FOREIGN KEY (`idedm_a`) REFERENCES `comptedm_a` (`idedm_a`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `testeuredm_n`
--
ALTER TABLE `testeuredm_n`
  ADD CONSTRAINT `TesteurEDM_N_ComptEDM_N_FK` FOREIGN KEY (`idedm_n`) REFERENCES `comptedm_n` (`idedm_n`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
