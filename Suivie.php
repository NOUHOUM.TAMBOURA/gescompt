<?php 

class Suivie

{
	Private $idSuivie;
	private $kilometrage;
	private $dateSv;
	private $idVehicule;

	

public function __construct($id,$k,$d,$idv){
		$this->idSuivie =$id;
		$this->kilometrage =$k;
		$this->dateSv =$d;
		$this->idVehicule =$idv;
	}


	// Les getters
	public function getIdSuivie(){return $this->idSuivie;}
	public function getKilometrage(){return $this->kilometrage;}
	public function getDateSv(){return $this->dateSv;}
	public function getIdVehicule(){return $this->idVehicule;}
	
 
	// les setteurs
	public function setIdSuivie($idSuivie)
	{
		$this->idSuivie= $idSuivie;
	}
	public function setKilometrage($kilometrage)
	{
		$this->kilometrage = $kilometrage;
		
	}
	public function setDateSv($dateSv)
	{

		$this->dateSv = $dateSv;
		
	}
	public function setIdVehicule($idVehicule)
	{

		$this->idVehicule = $idVehicule;
	}


}



//====================================================vehicule suivie =======================================================

class ManagerSuivie
{
private $_db; // Instance de PDO
public function __construct($db)
{
$this->setDb($db);
}


public function setDb(PDO $db)
{
$this->_db = $db;
}





    //Ajouter un compteur
	public function ajoutersuivie(Suivie $suivie){
	  
	       $req = $this->_db->prepare("
	                     INSERT INTO suivie (`idSuivie`, `kilometrage`, `dateSv`, `idVehicule`) 
	                               VALUES (null,:p,:m,:t)");
	   
	    	$req-> bindValue(':p',$suivie->getKilometrage());
	    	$req-> bindValue(':m',$suivie->getDateSv());
	    	$req-> bindValue(':t',$suivie->getIdVehicule());	 	    	
	    	
	    	$req->execute();
	    		
	    	}


	    	  public function Affichersuivie($ids)
{
             $req = array();
	    	$requete = " SELECT* FROM suivie,vehicule where vehicule.idVehicule=suivie.idVehicule and suivie.idVehicule= ".$ids;
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $obj= new Suivie ($donnee['idSuivie'],$donnee['kilometrage'],$donnee['dateSv'],$donnee['idVehicule']); 
	    	    $obj->setIdSuivie($donnee['idSuivie']);
	    	    $req[]=$obj;
	    	   
	    	}
	    	return $req;    
}


/*	  public function Affichervehicule($id)
{
             $req = array();
	    	$requete = " SELECT* FROM vehicule where vehicule.idVehicule=".$id;
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $obj= new Vehicule ($donnee['idVehicule'],$donnee['plaque'],$donnee['modele'],$donnee['type'],$donnee['numeroSerie'],$donnee['proprietaire']); 
	    	    $obj->setIdVehicule($donnee['idVehicule']);
	    	    $req[]=$obj;
	    	   
	    	}
	    	return $req;    
}*/

  //supprimer un objet 
public function supprimer($id)
{
$this->_db->exec("DELETE FROM suivie WHERE idSuivie=".$id);
}//fin supprimer*/


//recherche pour afficher un objet
		 public function chercher($suivie)
{
             $req = array();
	    	$requete = (" SELECT  *  FROM suivie where suivie.idSuivie=".$suivie);
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$obj= new Suivie ($donnee['idSuivie'],$donnee['kilometrage'],$donnee['dateSv'],$donnee['idVehicule']); 
	    	    $obj->setIdSuivie($donnee['idSuivie']);
	    	    $req[]=$obj;
	    	
	    	}
	    	return $req;   
}


 public function modifier($suivie) {

	    $req= $this->_db ->prepare("UPDATE suivie set kilometrage= :id, dateSv= :v where idSuivie =".$suivie);

	    	$req-> bindValue(':id',$_POST['Kilometrage']);
	    	$req-> bindValue(':v',$_POST['date']);
	   
			$req->execute();
		
		}


	}



 ?>