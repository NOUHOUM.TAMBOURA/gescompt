
<?php 

class Entretien

{
	Private $identretien;
	private $nombreEntretien;
	private $dateE;
	private $montant;
	private $idVehicule;
	

public function __construct($id,$n,$de,$m,$idv){
		$this->identretien =$id;
		$this->nombreEntretien =$n;
		$this->dateE =$de;
		$this->montant =$m;
		$this->idVehicule =$idv;
	}


	// Les getters
	public function getIdentretien(){return $this->identretien;}
	public function getNombreEntretien(){return $this->nombreEntretien;}
	public function getDateE(){return $this->dateE;}
	public function getMontant(){return $this->montant;}
	public function getIdVehicule(){return $this->idVehicule;} 
	
 
	// les setteurs
	public function setIdentretien($identretien)
	{
		$this->identretien= $identretien;
	}
	public function setNombreEntretien($nombreEntretien)
	{
		$this->nombreEntretien = $nombreEntretien;
		
	}
	public function setDateE($dateE)
	{

		$this->dateE = $dateE;
		
	}
	public function setMontant($montant)
	{

		$this->montant = $montant;
	}
	public function setIdVehicule($idVehicule)
	{

		$this->idVehicule = $idVehicule;
	}
	

}



//====================================================Entretien manager =======================================================

class ManagerEntretien
{
private $_db; // Instance de PDO
public function __construct($db)
{
$this->setDb($db);
}


public function setDb(PDO $db)
{
$this->_db = $db;
}



    //Ajouter un compteur
	public function ajouterEntretien(Entretien $entretien){
	  
	       $req = $this->_db->prepare("
	                     INSERT INTO entretien (`identretien`, `nombreEntretien`, `dateE`, `montant`, `idVehicule`) 
	                               VALUES (null,:p,:m,:t,:pr)");
	   
	    	$req-> bindValue(':p',$entretien->getNombreEntretien());
	    	$req-> bindValue(':m',$entretien->getDateE());
	    	$req-> bindValue(':t',$entretien->getMontant());
	    	$req-> bindValue(':pr',$entretien->getIdVehicule());	 	    	
	    	
	    	$req->execute();
	    		
	    	}


	    	  public function Afficherentretien($id)
{
             $req = array();
	    	$requete = " SELECT* FROM entretien,vehicule where entretien.idVehicule=vehicule.idVehicule and entretien.idVehicule=".$id;
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $obj= new Entretien ($donnee['identretien'],$donnee['nombreEntretien'],$donnee['dateE'],$donnee['montant'],$donnee['idVehicule']); 
	    	    $obj->setIdentretien($donnee['identretien']);
	    	    $req[]=$obj;
	    	   
	    	}
	    	return $req;    
}

/*
	  public function Affichervehicule($id)
{
             $req = array();
	    	$requete = " SELECT* FROM vehicule where vehicule.idVehicule=".$id;
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $obj= new Vehicule ($donnee['idVehicule'],$donnee['plaque'],$donnee['modele'],$donnee['type'],$donnee['numeroSerie'],$donnee['proprietaire']); 
	    	    $obj->setIdVehicule($donnee['idVehicule']);
	    	    $req[]=$obj;
	    	   
	    	}
	    	return $req;    
}*/


  //supprimer un objet 
public function supprimer($id)
{
$this->_db->exec("DELETE FROM entretien WHERE identretien=".$id);
}//fin supprimer*/




	//recherche pour afficher un objet
		 public function chercher($entretien)
{
             $req = array();
	    	$requete = (" SELECT  *  FROM entretien where identretien=".$entretien);
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$obj= new Entretien ($donnee['identretien'],$donnee['nombreEntretien'],$donnee['dateE'],$donnee['montant'],$donnee['idVehicule']); 
	    	    $obj->setIdentretien($donnee['identretien']);
	    	    $req[]=$obj;
	    	    
	    	    
	    	
	    	}
	    	return $req;   
}



//modifier un objet avec son id en parametre 
 public function modifier($entretien) {

	    $req= $this->_db ->prepare("UPDATE entretien set nombreEntretien= :dr, dateE = :hr,montant= :con  where identretien =".$entretien);

	    	$req-> bindValue(':dr',$_POST['nomEntretient']);
	    	$req-> bindValue(':hr',$_POST['date']);
	    	$req-> bindValue(':con',$_POST['montant']);	
			$req->execute();
		
		}










/*
  public function Affichervehicule($id)
{
             $req = array();
	    	$requete = " SELECT* FROM vehicule where idVehicule=".$id;
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $obj= new Vehicule ($donnee['idVehicule'],$donnee['plaque'],$donnee['modele'],$donnee['type'],$donnee['numeroSerie'],$donnee['proprietaire']); 
	    	    $obj->setIdVehicule($donnee['idVehicule']);
	    	    $req[]=$obj;
	    	   
	    	}
	    	return $req;    
}*/


	}


 ?>


