<?php 

class TesteurEdmA

{
	Private $idtesteur;
	private $lieu;
	private $ville;
	private $nom;
	private $idedm_a;

    public function  __construct($id,$lieu,$ville,$nom,$idcompteur){
    	$this->idtesteur =$id;
		$this->lieu =$lieu;
		$this->ville =$ville;
		$this->nom =$nom;
		$this->idedm_a =$idcompteur;
	}



	// Les getters
	public function getIdtesteur(){return $this->idtesteur;}
	public function getLieu(){return $this->lieu;}
	public function getVille(){return $this->ville;}
	public function getNom(){return $this->nom;} 
	public function getIdedm_a(){return $this->idedm_a;}  
	
 
	// les setteurs
	public function setIdtesteur($idtesteur)
	{
		$idtesteur = (int)$idtesteur;
		$this->idtesteur = $idtesteur;
	}
	public function setLieu($lieu)
	{

	if (is_string($lieu))
		{
		$this->lieu = $lieu;
		}
	}
	public function setVille($ville)
	{

	if (is_string($ville))
		{
		$this->ville = $ville;
		}
	}
	
	public function setNom($nom)
	{

	if (is_string($nom))
		{
		$this->nom = $nom;
		}
	}
	public function setIdedm_a($idedm_a)
	{

	if (is_string($idedm_a))
		{
		$this->idedm_a = $idedm_a;
		}
	}
	
	

}


 ?>