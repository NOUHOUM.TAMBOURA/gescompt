<?php 
  // include("connexion.php");
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Agence Immobilière Miankala</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
 <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->




  <link rel="stylesheet" type="text/css" href="lib/bootstrap-fileupload/bootstrap-fileupload.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="lib/bootstrap-datetimepicker/datertimepicker.css" />
  <link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper" >

 
 <?php
 
 include("header.php");
 
  ?>


   <!-- ========================= -->

<?php 
require("autoriser.php");
?>
<!-- Left side column. contains the logo and sidebar -->
 <!-- <body> -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
        </div>
        <div class="pull-left info">
         <!--  <p>Moussa DIARRA</p> -->
          <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
        </div>
      </div>
      <p style="text-align: center; color: white;"><?php echo strtoupper($nom)." ".strtoupper($prenom);  ?></p>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Rechercher...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">DECONNEXION</li>
         <li><a href="../index.php"><i class="fa fa-dashboard"></i> <span>Se deconnecter</span></a></li>
        <li class="header">MENU PRINCIPAL</li>
         <li><a href="index.php"><i class="fa fa-dashboard"></i> <span>Accueil</span></a></li>
      
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Utilisateur</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
            <li><a href="form_agent.php"><i class="fa fa-circle-o"></i> Nouveau</a></li>
            <li><a href="list_agent.php"><i class="fa fa-circle-o"></i> Liste</a></li>
            <!-- <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Nomination</a></li> -->
            
          </ul>
        </li>
        <li class="treeview active">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Proprietaire</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="form_proprietaire.php"><i class="fa fa-circle-o"></i> Nouveau</a></li>
            <li><a href="list_proprietaire.php"><i class="fa fa-circle-o"></i> Liste</a></li>
            
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Locataire</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="form_locataire.php"><i class="fa fa-circle-o"></i> Nouveau</a></li>
            <li><a href="list_locataire.php"><i class="fa fa-circle-o"></i> Liste</a></li>
           <!--  <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li> -->
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Maison</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="form_maison.php"><i class="fa fa-circle-o"></i> Ajouter une maison</a></li>
            <li class=""><a href="list_maison.php"><i class="fa fa-circle-o"></i> Liste</a></li>
          
          </ul>
        </li>

          <li class="treeview ">
          <a href="#">
            <i class="fa fa-table"></i> <span>Appartement</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="form_appartement.php"><i class="fa fa-circle-o"></i> Nouveau</a></li>
            <li><a href="list_appartement.php"><i class="fa fa-circle-o"></i> Liste</a></li>
            <li><a href="louer.php"><i class="fa fa-circle-o"></i>  Location (App-Loc)</a></li>
            <li><a href="list_louer.php"><i class="fa fa-circle-o"></i> Liste(App-Loc)</a></li>
          </ul>
        </li>

        
        <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Contrat proprietaire
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="form_appartenance.php"><i class="fa fa-circle-o"></i> Location (Mais-App)</a></li>
                <li><a href="list_appartenance.php"><i class="fa fa-circle-o"></i> Liste</a></li>
               
              </ul>
      </li>
     
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Recouvrement</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="form_payerD.php"><i class="fa fa-circle-o"></i> Payer</a></li>
            <li><a href="list_payes.php"><i class="fa fa-circle-o"></i> Liste des payements</a></li>
            <!-- <li><a href="list_payer.php"><i class="fa fa-circle-o"></i> Liste de payement par date</a></li> -->
          
          </ul>
        </li>
       
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>


  <!-- ================= -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <i class="fa fa-users"></i> Nouveau Proprietaire 
        <!-- <small>Preview</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Acceuil</a></li>
        <li><a href="#">Formulaire</a></li>
        <!-- <li class="active">Advanced Elements</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Reserver pour les informations personnelles du proprietaire </h3>

          <div class="box-tools pull-right">
           <!--  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form action="" method="POST" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                  <label for="nom">Nom</label>
                  <input type="text" class="form-control" id="nom" placeholder="Nom" name="nom" required="">
                </div>
                <div class="form-group">
                  <label for="adresse">Adresse</label>
                  <input type="text" class="form-control" id="adresse" placeholder="Adresse" name="adresse" >
                </div>
                
              <!-- /.form-group -->
            </div>
            <!-- /.col -->
            <div class="col-md-6">
              <div class="form-group">
                  <label for="prenom">Prenom</label>
                  <input type="text" class="form-control" id="prenom" placeholder="Prenom" name="prenom" required="">
                </div>
                <div class="form-group">
                  <label for="prof">Profession</label>
                  <input type="text" class="form-control" id="prof" placeholder="Profession" name="prof">
                </div>
              <!-- /.form-group -->
            </div>
             <div class="col-md-6">
              <div class="form-group">
                  <label for="cont1">Contact1</label>
                  <input type="text" class="form-control" id="cont1" placeholder="Contact" name="cont1" >
                </div>
                <div class="form-group">
                  <label for="mail">Email</label>
                  <input type="text" class="form-control" id="mail" placeholder="Email" name="mail">
                </div>
              <!-- /.form-group -->
            </div>
             <div class="col-md-6">
              <div class="form-group">
                  <label for="cont2">Contact2</label>
                  <input type="text" class="form-control" id="cont2" placeholder="Contact" name="cont2">
                </div>
                <div class="form-group">
                  <label for="pass">N° carte d'identification</label>
                  <input type="text" class="form-control" id="pass" placeholder="Numéro matricule" name="num" required="">
                </div>
              <!-- /.form-group -->
            </div>
             <div class="col-md-6">
              <div class="form-group">
                  <label>Moralite</label>
                  <textarea class="form-control" rows="3" placeholder="Entrer ..." name="moral"></textarea>
                </div>
                <div class="">
                <button type="submit" class="btn btn-primary" name="enregistrer">Enregistrer</button>
                <button type="submit" class="btn btn-warning" name="annuler">Annuler</button>
                <button type="submit" class="btn btn-danger" name="retour">Retour</button>
                
              </div>
                
              <!-- /.form-group -->
            </div>
             <div class="col-md-6">
              
               

                <div class="form-group ">
                 <label for="carte">Carte d'identité</label>
                 
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" alt="" />
                      </div>
                      <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                      <div>
                        <span class="btn btn-primary btn-file">
                          <span class="fileupload-new"><i class="fa fa-paperclip"></i> Choisir une image</span>
                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Changer</span>
                        <input type="file" class="default" name="carte" id="carte" />
                        </span>
                       
                      </div>
                    </div>
                   
                   
                  
                </div>

              <!-- /.form-group -->
            </div>
           
            <!-- /.col -->
          </div>

        </form>

<?php if (isset($_POST['enregistrer'])) {
  

  $ud= new DaoProprietaire($db);
                $num =htmlspecialchars ( $_POST['num']);
                $nom =htmlspecialchars ( $_POST['nom']);
                $prenom =htmlspecialchars ( $_POST['prenom']);
                $adresse =htmlspecialchars ( $_POST['adresse']);
                $cont1 =htmlspecialchars ( $_POST['cont1']);
                $cont2 =htmlspecialchars ( $_POST['cont2']);
                $mail =htmlspecialchars ( $_POST['mail']);
                // echo  $photo =htmlspecialchars ( $_FILES['carte']);
                $prof =htmlspecialchars ( $_POST['prof']);
                $moral =htmlspecialchars ( $_POST['moral']);

    // ===========================================================================================================================


  $imageprofil = $_FILES['carte']['name'];
// Je crée un array dans lequel figurent seulement les extensions acceptées, avec le type MIME qui leur est associé (qui peut varier sous IE et qu'on va donc devoir différencier) :
$ListeExtension = array('jpg' => 'image/jpeg', 'jpeg' => 'image/jpeg', 'png' => 'image/png', 'gif' => 'image/gif');
$ListeExtensionIE = array('jpg' => 'image/pjpg', 'jpeg'=>'image/pjpeg'); // Il fallait une nouvelle fois qu'IE se différencie. 
// Je vérifie l'extension présumée du fichier :
$ExtensionPresumee = explode('.', $imageprofil);
$ExtensionPresumee = strtolower($ExtensionPresumee[count($ExtensionPresumee)-1]);
if ($ExtensionPresumee == 'jpg' || $ExtensionPresumee == 'jpeg' || $ExtensionPresumee == 'pjpg' || $ExtensionPresumee == 'pjpeg' || $ExtensionPresumee == 'gif' || $ExtensionPresumee == 'png') 
{
    // verification du type MIME
$ImageNews = getimagesize($_FILES['carte']['tmp_name']);
if($ImageNews['mime'] == $ListeExtension[$ExtensionPresumee] || $ImageNews['mime'] == $ListeExtensionIE[$ExtensionPresumee]) 
{
    // creation d'une copie de l'image profil 
$ImageChoisie = imagecreatefromjpeg($_FILES['carte']['tmp_name']);
    // recuperation des dimensioins de l'image 
    $TailleImageChoisie = getimagesize($_FILES['carte']['tmp_name']);
    // nouvelle dimensions
$NouvelleLargeur = 250;

$Reduction = ( ($NouvelleLargeur * 100)/$TailleImageChoisie[0] );

$NouvelleHauteur = ( ($TailleImageChoisie[1] * $Reduction)/100 ); 
//  creation de l'image miniature 
$NouvelleImage = imagecreatetruecolor($NouvelleLargeur , $NouvelleHauteur) or die ("Erreur");

imagecopyresampled($NouvelleImage , $ImageChoisie, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0],$TailleImageChoisie[1]); 
//  detruire l'ancienne image 
imagedestroy($ImageChoisie);

// sauvegarder le ficher sur le server 
// $NomImageChoisie = explode('.', $ImageNews);
$NomImageExploitable = time();
// enregistrement 
imagejpeg($NouvelleImage , 'img/jpeg/proprietaire/'.$NomImageExploitable.'.'.$ExtensionPresumee, 100);

 $nomfichier= $NomImageExploitable.'.'.$ExtensionPresumee;

}
}

if (empty($nomfichier)) {
  $nomfichier="inconnu";
}


    // ===========================================================================================================================



              $proprietaire=new Proprietaire(array(
                'numPro'=>$num,
                'nomPro'=>$nom,
                'prenomPro'=>$prenom,
                'adressePro'=>$adresse,
                'contact1Pro'=>$cont1,
                'contact2Pro'=>$cont2,
                'mailPro'=>$mail,
                'photoIdPro'=>$nomfichier,
                'professionPro'=>$prof,
                'moralitePro'=>$moral
        ));

             // appelle la fonction ajouteruser du dao
           $ud->ajouter_pro($proprietaire);

           ?>
           <!-- message afficher si tout fonctionne bien -->
           <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>Nouveau proprietaire enregistrer avec Succès.</strong> 
            </div>
            
            <?php







} ?>

        
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
     
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2019-2020 <a href="https://adminlte.io">Xpertpro GROUP</a>.</strong> Tous droits reservés
  </footer>

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->




<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<!-- <script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> -->
<!-- bootstrap color picker -->
<!-- <script src="../../bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
 --><!-- bootstrap time picker -->
<!-- <script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script> -->
<script type="text/javascript"   src="lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript"   src="lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script src="lib/advanced-form-components.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, locale: { format: 'MM/DD/YYYY hh:mm A' }})
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
