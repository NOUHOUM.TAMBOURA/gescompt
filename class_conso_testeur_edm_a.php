<?php 

class ConsoTesteurEdmA

{
	Private $idcons1;
	Private $index1;
	private $dateReleve;
	private $heureReleve;
	private $dateHeureReleve;
	private $consommation;
	Private $idtesteur;
	Private $idcons;
	


	public function  __construct($index,$date,$heure,$drv,$consommation,$id,$idcons){
		$this->index1 =$index;
		$this->dateReleve =$date;
		$this->heureReleve =$heure;
		$this->dateHeureReleve =$drv;
		$this->consommation =$consommation;
		$this->idtesteur =$id;
		$this->idcons =$idcons;
	}



	// Les getters
	public function getIdcons1(){return $this->idcons;}
	public function getIndex1(){return $this->index1;}
	public function getDateReleve(){return $this->dateReleve;} 
	public function getHeureReleve(){return $this->heureReleve;} 
	public function getDateHeureReleve(){return $this->dateHeureReleve;} 
	public function getConsommation(){return $this->consommation;} 
	public function getIdtesteur(){return $this->idtesteur;}
	public function getIdcons(){return $this->idcons;} 
	
 
	// les setteurs
	public function setIdcons1($idcons1)
	{
		$idcons1 = (int)$idcons1;
		$this->idcons1 = $idcons1;
	}

	public function setDateReleve($dateReleve)
	{
		$this->dateReleve = $dateReleve;
	
	}

	public function setHeureReleve($heureReleve)
	{
		$this->heureReleve = $heureReleve;
	
	}

    public function setDateHeureReleve($dateHeureReleve)
	{
		$this->dateHeureReleve = $dateHeureReleve;
	
	}

	public function setConsommation($consommation)
	{
		$this->consommation = $consommation;
	
	}

	public function setIndex1($index1)
	{
		$this->index1 = $index1;
	
	}
	
	public function setIdtesteur($idtesteur)
	{
		$this->idtesteur = $idtesteur;
	
	}
	
	public function setIdcons($idcons)
	{
		$this->idcons = $idcons;
	
	}
	

}


 ?>