<?php 

class ConsoTesteurSomagep

{
	Private $idconsotesteur;
	private $dateReleve;
	private $heureReleve;
	private $dateheurereleve;
	private $consommation;
	private $idtesteur;
	private $idcons;
	


	public function  __construct($dateReleve,$heureReleve,$dhr,$consommation,$idtesteur,$idcons){
		$this->dateReleve =$dateReleve;
		$this->heureReleve =$heureReleve;
		$this->dateheurereleve =$dhr;
		$this->consommation =$consommation;
		$this->idtesteur =$idtesteur;
		$this->idcons =$idcons;
	}



	// Les getters
	public function getIdconsotesteur(){return $this->idconsotesteur;}
	public function getDateReleve(){return $this->dateReleve;}
	public function getHeureReleve(){return $this->heureReleve;} 
	public function getDateheurereleve(){return $this->dateheurereleve;} 
	public function getConsommation(){return $this->consommation;}
	public function getIdtesteur(){return $this->idtesteur;}
	public function getIdcons(){return $this->idcons;}   
	
 
	// les setteurs
	public function setIdconsotesteur($idconsotesteur)
	{
		$idconsotesteur = (int)$idconsotesteur;
		$this->idconsotesteur = $idconsotesteur;
	}

	public function setDateReleve($dateReleve)
	{
		$this->dateReleve = $dateReleve;
	}

	public function setHeureReleve($heureReleve)
	{
		$this->heureReleve = $heureReleve;
	
	}


	public function setDateheurereleve($dhr)
	{
		$this->dhr = $dhr;
	
	}
	

	public function setConsommation($consommation)
	{
		$this->consommation = $consommation;
	
	}

	public function setIdtesteur($idtesteur)
	{
		$this->idtesteur = $idtesteur;
	
	}

	public function setIdcons($idcons)
	{
		$this->idcons = $idcons;
	
	}
	
	

}


 ?>