<?php 

class CompteurSomagep

{
	Private $idCompts;
	private $ville;
	private $localisation;
	private $nom;
	

public function  __construct($id,$ville,$localisation,$nom){
		$this->idCompts =$id;
		$this->ville =$ville;
		$this->localisation =$localisation;
		$this->nom =$nom;
	}


	// Les getters
	public function getIdCompts(){return $this->idCompts;}
	public function getVille(){return $this->ville;}
	public function getLocalisation(){return $this->localisation;}
	public function getNom(){return $this->nom;} 
	
 
	// les setteurs
	public function setIdCompts($idCompts)
	{
		$this->idCompts = $idCompts;
	}
	public function setVille($ville)
	{

	if (is_string($ville))
		{
		$this->ville = $ville;
		}
	}
	public function setLocalisation($localisation)
	{

	if (is_string($localisation))
		{
		$this->localisation = $localisation;
		}
	}
	public function setNom($nom)
	{

	if (is_string($nom))
		{
		$this->nom = $nom;
		}
	}
	

}


 ?>