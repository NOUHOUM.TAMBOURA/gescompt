<?php 

class TesteurCompteurSomagep

{
	Private $idtesteur;
	private $ville;
	private $localisation;
	private $nom;
	private $idcomptS;

public function  __construct($id,$ville,$localisation,$nom,$idcompteur){
		$this->idtesteur =$id;
		$this->ville =$ville;
		$this->localisation =$localisation;
		$this->nom =$nom;
		$this->idcomptS =$idcompteur;
	}

	

	// Les getters
	public function getIdtesteur(){return $this->idtesteur;}
	public function getVille(){return $this->ville;}
	public function getLocalisation(){return $this->localisation;}
	public function getNom(){return $this->nom;}
	public function getIdcomptS(){return $this->idcomptS;}  
	
 
	// les setteurs
	public function setIdtesteur($idtesteur)
	{
		$this->idtesteur = $idtesteur;
	}
	public function setVille($ville)
	{

	if (is_string($ville))
		{
		$this->ville = $ville;
		}
	}
	public function setLocalisation($localisation)
	{

	if (is_string($localisation))
		{
		$this->localisation = $localisation;
		}
	}
	public function setNom($nom)
	{

	if (is_string($nom))
		{
		$this->nom = $nom;
		}
	}

	public function setIdcompteur($idcompteur)
	{

	if (is_string($idcompteur))
		{
		$this->idcompteur = $idcompteur;
		}
	}
	

}


 ?>