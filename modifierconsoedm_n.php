<?php
include('autorisation.php');
 require('managercreationcons_edm_n.php');
  require('managercreationedm_n.php');
 include('config.php');

    // une instance de la dao
$ud=new ManagerConsoEdm_n($db);
if(isset($_GET['modifier'])){ 
   $id=$_GET['modifier'];

$chercher= $ud->chercher($id);

foreach ($chercher as $value) {                   
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="icon" href="img/logo1.png">

  <title>gescompt</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
     <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon rotate-n-25">
          <i class=""></i>
        </div>
        <div class="sidebar-brand-text mx-3"><?php echo strtoupper($prenom ."<br>" .$nom);?> 

          </div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->

        <?php if ($_SESSION['statut']==1) { ?> 
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th>
       <li class="nav-item active">
        <a class="nav-link" href="utilisateur.php">
          <i class=""></i>
         <span class="text-center"> <h2 class="btn btn-danger col-sm-12">Utilisateur</h2></span></a>
      </li>
      <?php }  ?>


      <!-- gestion parc auto -->
        <?php if ($_SESSION['statut']==4 ||  ($_SESSION['statut']==1)) { ?> 
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th>
       <li class="nav-item active">
        <a class="nav-link" href="ajoutervehicule.php">
          <i class=""></i>
         <span class="text-center"> <h2 class="btn btn-info col-sm-12">Parc Auto</h2></span></a>
      </li>
      <?php }  ?>

         <!-- gestion groupe electrogene -->
        <?php if ($_SESSION['statut']==4 ||  ($_SESSION['statut']==1)) { ?> 
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th>
       <li class="nav-item active">
        <a class="nav-link" href="ajoutergenerateur.php">
          <i class=""></i>
         <span class="text-center"> <h2 class="btn btn-warning col-sm-12">Génerateur</h2></span></a>
      </li>
      <?php }  ?>



      <li class="nav-item active">
        <a class="nav-link" href="index.php">
          <i class=""></i>
         <span class="text-center text-uppercase"> <h2 class="btn btn-info col-sm-12">ACCUEIL </h2></span></a>
      </li>


<?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        GESTION DES COMPTEURS
      </div>

      <?php }  ?>

      <!-- Nav Item - Pages pour la creation des compteurs -->
      <?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
                  

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Creation</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
            <a class="collapse-item btn btn-primary" href="creationsomagep.php">Somagep</a><br>
            <a class="collapse-item btn btn-success" href="creation_edm_numerique.php"> EDM Numérique</a><br>
            <a class="collapse-item btn btn-info" href="creation_edm_analogique.php">EDM Analogique</a><br>
            <a class="collapse-item btn btn-warning" href="cration_electromenager.php"> Electromenager</a>
          </div>
        </div>
      </li>
      <?php }  ?>

<?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
       <hr class="sidebar-divider">
      <!-- Nav Item - pour la consommation des compteurs -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Releve / Index</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
            <a class="collapse-item btn btn-primary" href="consommation_somagep.php">Somagep</a><br>
            <a class="collapse-item btn btn-success" href="consommation_edm_numerique.php">EDM Numérique</a><br>
            <a class="collapse-item btn btn-info" href="consommation_edm_analogique.php">EDM Analogique</a><br>
            <a class="collapse-item btn btn-warning" href="consoelectromenager.php">Electromenager</a>
          </div>
        </div>
      </li>
      <?php }  ?>
      <!-- Divider -->


      <?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 

      <hr class="sidebar-divider">
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Statistique / Conso</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
           <!--  <a class="collapse-item" href="statistiques.php">Statistique</a> -->
            <a class="collapse-item btn btn-primary" href="statistiques_somagep.php">Somagep</a><br>
            <a class="collapse-item btn btn-success" href="statistiques_edm_numerique.php">EDM Numérique</a><br>
            <a class="collapse-item btn btn-info" href="statistiques_edm_analogique.php">EDM Analogique</a>
           <!--  <a class="collapse-item" href="statistiques_electromenager.php">Electromenager</a> -->
           
          </div>
        </div>
      </li>
      <?php }  ?>
  

    

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

         

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Recherche" aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

          
            <!-- <div class="topbar-divider d-none d-sm-block"></div>
 -->
          

          </ul>
<h2 style="margin-right: 30%;" class="text-primary">Système Informatique de Gestion des Compteurs</h2>
              <a href="deconnexion.php"><button type="button" class="btn btn-danger">Deconnexion</button></a>
        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
         <!--  <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800"></h1>
            <a href="creationsomageptesteur.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i>nouveau powermetter</a>
          </div>
 -->
          <!-- les contenues -->
           
<!-- ==================================================================================== -->
<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-10">
            <div class="p-5">
               <div class="text-center">
                <h1  class="text-primary">Modifier Consommation EDM Numérique</h1>
              </div><br><br>
              <form  class="user" method="POST" action="">

                <div>
                <div class="form-group row">
                      <div class="col-sm-6">
                        <span class="text-primary">Heure Creuse:</span>
                        <input type="text" value="<?php echo $value->getIndexheurecreuse() ?>" class="form-control " id="exampleFirstName" placeholder="index heure Creuse" name="heurecreuse">
                      </div>
                      <div class="col-sm-6">
                        <span class="text-primary">Heure pleine :</span>
                        <input type="text" value="<?php echo $value->getIndexheurepleine() ?>" class="form-control " id="exampleFirstName" placeholder="index heure Pleine" name="heurepleine">
                      </div>
                </div><br>

                  <div class="form-group row">
                      <div class="col-sm-6">
                        <span class="text-primary">Heure Pointe :</span>
                        <input type="text" value="<?php echo $value->getIndexheurepoint() ?>" class="form-control " id="exampleFirstName" placeholder="index heure Pointe" name="heurepointe">
                      </div>
                      <div class="col-sm-6">
                        <span class="text-primary">Date du Releve :</span>
                        <input type="date" value="<?php echo $value->getDateReleve() ?>" class="form-control " id="exampleLastName" placeholder="date releve" name="datereleve">
                      </div>
                  </div><br>

                  <div class="form-group row">
                      <div class="col-sm-6">
                        <span class="text-primary">Heure du Releve :</span>
                        <input type="time" value="<?php echo $value->getHeureReleve() ?>" class="form-control " id="exampleInputEmail" placeholder="heure releve" name="heurereleve">
                     </div>
                      <div class="col-sm-6">
                        <span class="text-primary">Identifiant du Compteur :</span>
                        <select type="text" value="<?php echo $value->getIdEdm_n() ?>" class="form-control " id="exampleInputEmail" placeholder="Identifiant compteurs" name="idcompteur">
                          <option disabled="disabled" selected="selected">Choisir un Compteur</option>

                        <?php 
                          
                           $conso=new Manageredm_n($db);
                          //methode afficher 
                          $pub = $conso->Afficheredm_n();
                          foreach ($pub as $value) {
                            $val1=$value->getIdEdm_n();
                                                ?>
                              <option value="<?php echo $val1 ?>">
                                <?php   
                                  echo $value->getIdEdm_n()." | ".$value->getVille()." | ".$value->getNom(); ?> </option>
                                       <?php } // fin foreach
                                          
                                       
                      ?>
                        </select>
                        <!-- <input type="text" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="Identifiant compteurs"> -->
                      </div>
                  </div>
                  </div>
                </div>
            

               <div class="row" style="margin:0px 0px 30px 30px ;">
            <div class="col-md-6">
              <button type="submit" class="btn btn-primary" name="modifier" style="margin-right: 30px;">
            Confirmer
              </button>
               <a href="consommation_edm_numerique.php" class="btn btn-danger"><i class=""></i>Annuler</a>
            </div>
        </div>
              </form>



           <?php  
         }
       }
                if(isset($_POST['modifier'])){
                  $ud->modifier($id); 
                  
                  ?>
                  <script type="text/javascript">
                    document.location.replace('listeconsoedm_n.php')
                  </script> 

                  <?php 
              }

           ?>
         
          
              
          </div>
        </div>
      </div>
    </div>

  </div>
 <!-- Donnee du tableau -->
                 <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>


          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- DataTales Example -->
              <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>


          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
       
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy;  2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>
        <!-- /.container-fluid -->

      </div>

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>



</body>



</html>
