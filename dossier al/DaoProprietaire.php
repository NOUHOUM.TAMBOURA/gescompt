
<?php
require("../ClassModel/Proprietaire.php");
class DaoProprietaire
{
private $_db; // Instance de PDO
public function __construct($db)
{
$this->setDb($db);
}


public function setDb(PDO $db)
{
$this->_db = $db;
}

    //Ajouter utilisateur
	public function ajouter_pro(proprietaire $loc){
	    	
	    $req = $this->_db->prepare("INSERT INTO proprietaire (numPro,nomPro,prenomPro,adressePro,contact1Pro,contact2Pro,mailPro,photoIdPro,professionPro,moralitePro )
	    	VALUES
	    		(:numpro,:nom,:prenom,:add,:cont1,:cont2,:mail,:pht,:prof,:moral)" );

	    	// $req-> bindValue(':idService',$rep->getIdService());
	    	//$req-> bindValue(':datereponse',$rep->getDate_reponse());
	    	$req-> bindValue(':numpro',$loc->getNumPro());
	    	$req-> bindValue(':nom',$loc->getNomPro());
	    	$req-> bindValue(':prenom',$loc->getPrenomPro());
	    	$req-> bindValue(':add',$loc->getAdressePro());
	    	$req-> bindValue(':cont1',$loc->getContact1Pro());
	    	$req-> bindValue(':cont2',$loc->getContact2Pro());
	    	$req-> bindValue(':mail',$loc->getMailPro());
	    	$req-> bindValue(':pht',$loc->getPhotoIdPro());
	    	$req-> bindValue(':prof',$loc->getProfessionPro());
	    	$req-> bindValue(':moral',$loc->getMoralitePro());
	    	
	    	   	
	    	 	    	
	    	$req->execute();

	    	if ($req) 
	    	{
	    		$message[]= 'succes';
	    	}else{
	    	$message[]= 'echec';}
	    }
	   //affichage
	    public function afficher_pro()
{
             $req = array();
	    	$requete = " SELECT* FROM proprietaire";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	      
	   $obj = new Proprietaire ($donnee); 
	    	    // $obj->setIdArt($donnee['idArt']);
	    	$req[] =  $obj;
	    	}
	    	return $req;
	    	
	    	
	    	
	    	
	    
}

//supprimer un objet 
public function delete( $pro)
{
$this->_db->exec("DELETE FROM proprietaire WHERE numPro ='$pro'");
}

//modification du dossier d'un proprietaire avec photo
 public function update ($idserv, $photo) {

	    $req= $this->_db ->prepare("UPDATE proprietaire set numPro= :num, nomPro= :nom, prenomPro= :prenom, adressePro = :add, contact1Pro = :cont1, contact2Pro = :cont2, mailPro = :mail, photoIdPro ='$photo', professionPro = :prof, moralitePro = :moral where numPro ='$idserv'") ;
	     $req->bindValue(':num', $_POST['num']);
	    $req->bindValue(':nom', $_POST['nom']);
	    $req->bindValue(':prenom', $_POST['prenom']);
	    $req->bindValue(':add', $_POST['adresse']);
	    $req->bindValue(':cont1', $_POST['cont1']);
	    $req->bindValue(':cont2', $_POST['cont2']);
	    $req->bindValue(':mail', $_POST['mail']);
	    // $req->bindValue(':pht', $_POST['libService']);
		$req->bindValue(':prof', $_POST['prof']);
		$req->bindValue(':moral', $_POST['moral']);
		

		//$req->bindValue(':msgsujet', $_POST['msgsujet']);
		

		$req->execute();
		
		}



// modification du dossier d'un proprietaire sans photo
 public function update1 ($idserv) {

	    $req= $this->_db ->prepare("UPDATE proprietaire set numPro= :num, nomPro= :nom, prenomPro= :prenom, adressePro = :add, contact1Pro = :cont1, contact2Pro = :cont2, mailPro = :mail, professionPro = :prof, moralitePro = :moral where numPro ='$idserv'") ;
	     $req->bindValue(':num', $_POST['num']);
	    $req->bindValue(':nom', $_POST['nom']);
	    $req->bindValue(':prenom', $_POST['prenom']);
	    $req->bindValue(':add', $_POST['adresse']);
	    $req->bindValue(':cont1', $_POST['cont1']);
	    $req->bindValue(':cont2', $_POST['cont2']);
	    $req->bindValue(':mail', $_POST['mail']);
	    // $req->bindValue(':pht', $_POST['libService']);
		$req->bindValue(':prof', $_POST['prof']);
		$req->bindValue(':moral', $_POST['moral']);
		

		//$req->bindValue(':msgsujet', $_POST['msgsujet']);
		

		$req->execute();
		
		}		
		//recherche pour afficher un objet
		 public function chercher($code)
{
             $req = array();
	    	$requete = (" SELECT* FROM proprietaire where numPro='$code'");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	 $obj = new Proprietaire ($donnee); 
	    	    // $obj->setIdArt($donnee['idArt']);
	    	$req[] =  $obj;
	    	}
	    	return $req;
	    	
	  	    
}

//fonction compter le nombre proprietaire.
   public function counte()
{
     return $this->_db->query("SELECT COUNT(*) FROM proprietaire")->fetchColumn();
}//fin compter




//afficher tout les reponses
		 public function afficherparsujets($idsuj)
{
               $req = array();
	    	$requete = " SELECT* FROM reponse where id_sujet=".$idsuj."  ORDER BY datereponse DESC ";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	      
	    $obj = new Forum_reponse ($donnee['message'],$donnee['datereponse'], $donnee['Id_user'],$donnee['id_sujet']); 
	    	    $obj->setId_reponse($donnee['id_reponse']);
	    	$req[] =  $obj;
	    	}
	    	return $req;
	    	
	    	
	    
}




//jointure
		 public function afficherparsujetjoint($idsuj)
{
               $req = array();
	    	$requete = " SELECT* FROM reponse,user where( (id_sujet=".$idsuj.")and (reponse.id_user=user.id_user)) ORDER BY datereponse DESC ";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    echo '<table width="80%"  align="center" >';
	    echo '<tr class="blanc"><th bgcolor="#621422" height="10px">'.strtoupper($donnee['login']) .'</th></font></tr>';
	    echo '<tr><td bgcolor="#fff2f2">'.$donnee['message'] .'</td></tr>';	
	       echo '<tr><td align="right">'.$donnee['datereponse'].'</td></tr>'; 
	       echo '<th bgcolor="#621422" height="2px"></th></tr>'; 
	       echo "</br>"; 
	    	}
	  
}
	}
?>