	
<?php
require("class_creation_compteur_edm_n.php");
require("class_creation_testeur_edm_n.php");
//require_once("connexion.php");

class Manageredm_n
{
private $_db; // Instance de PDO
public function __construct($db)
{
$this->setDb($db);
}


public function setDb(PDO $db)
{
$this->_db = $db;
}



// fonction qui permet de verifier si identifiant existe dans la base
	    public function verifier($id)
{
	$erreur=null;
$requete = (" SELECT* FROM comptedm_n where idedm_n='$id'");

	    	$list = $this->_db->query($requete);
	    	if ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {

	$erreur="<center><font color='red'><h3>Ce Compteur Existe deja!</h3></font></center>";
	
}//fin if
	return $erreur;	

}


    //Ajouter un compteur
	public function ajouteredm_n(CompteurEdmN $edmn){
	    	
	       $req = $this->_db->prepare("
	                     INSERT INTO comptedm_n (`idedm_n`, `ville`, `localisation`, `nom`) 
	                               VALUES (:id,:ville,:lieu,:nom)");
	   
	    	$req-> bindValue(':id',$edmn->getIdEdm_n());
	    	$req-> bindValue(':ville',$edmn->getVille());
	    	$req-> bindValue(':lieu',$edmn->getLocalisation());
	    	$req-> bindValue(':nom',$edmn->getNom());	 	    	
	    	/*$req-> bindValue(':structure',$user->getStructure());*/
	    	
	    	$req->execute();

	    }

 public function Afficher()
{
             $req = array();
	    	$requete = " SELECT* FROM comptedm_n";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $obj = new CompteurEdmN ($donnee['idedm_n'],$donnee['ville'],$donnee['localisation'],$donnee['nom']); 
	    	    $obj->setIdEdm_n($donnee['idedm_n']);
	    	    $req[]=$obj;
	    	}
	    	return $req;    
}

   //supprimer un objet 
public function supprimer($edm)
{
$this->_db->exec("DELETE FROM comptedm_n WHERE idedm_n='$edm'");
}//fin supprimer*/


//modifier un objet avec son id en parametre 
 public function modifier($somagep) {

	    $req= $this->_db ->prepare("UPDATE comptedm_n set idedm_n= :id, ville= :v, localisation = :l,nom= :n where idedm_n ='$somagep'");

	    	$req-> bindValue(':id',$_POST['identifiant']);
	    	$req-> bindValue(':v',$_POST['lieu']);
	    	$req-> bindValue(':l',$_POST['ville']);
	    	$req-> bindValue(':n',$_POST['nom']);
			$req->execute();
		
		}

		//recherche pour afficher un objet
		 public function chercher($somagep)
{
             $req = array();
	    	$requete = (" SELECT  *  FROM comptedm_n where idedm_n='$somagep' ");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$req[]= new CompteurEdmN ($donnee['idedm_n'],$donnee['ville'],$donnee['localisation'],$donnee['nom']); 
	    	
	    	}
	    	return $req;   
}

//===================================== methodes pour les testeurs===================================
 //Ajouter automatiquement le testeur du compteur qui ete creer
	public function ajoutertesteuredm_n(TesteurEdmN $edm){
	    	
	       $req = $this->_db->prepare("
	                     INSERT INTO testeuredm_n (`idtesteur`, `lieu`, `ville`, `nom`, `idedm_n`) 
	                               VALUES (:id,:ville,:lieu,:nom,:idtesteur)");

	   		$req-> bindValue(':id',$edm->getIdtesteur());
	    	$req-> bindValue(':ville',$edm->getLieu());
	    	$req-> bindValue(':lieu',$edm->getVille());
	    	$req-> bindValue(':nom',$edm->getNom());
	    	$req-> bindValue(':idtesteur',$edm->getIdedm_n());	 	    	
	    	
	    	$req->execute();
	    		
	    	}
	  //============================= methodes pour verifier ============================

 public function verifiertesteur($id)
{
	$erreur=null;
$requete = (" SELECT* FROM testeuredm_n where idedm_n='$id'");

	    	$list = $this->_db->query($requete);
	    	if ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {

	$erreur="<center><font color='red'><h3>Pas de testeur creer pour ce Compteur!</h3></font></center>";
	
}//fin if
	return $erreur;	

}
//recuprer la liste pour classe consommation testeur edm numerique
public function Affichertesteur()
{
             $req = array();
	    	$requete = " SELECT* FROM testeuredm_n";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $req[] = new TesteurEdmN ($donnee['idtesteur'],$donnee['lieu'],$donnee['ville'],$donnee['nom'],$donnee['idedm_n']); 
	    	    
	    	}
	    	return $req;    
}

// recuprer la liste pour classe consommation edm numerique
public function Afficheredm_n()
{
             $req = array();
	    	$requete = " SELECT* FROM comptedm_n";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $obj = new CompteurEdmN ($donnee['idedm_n'],$donnee['ville'],$donnee['localisation'],$donnee['nom']); 
	    	    $obj->setIdEdm_n($donnee['idedm_n']);
	    	    $req[]=$obj;
	    	}
	    	return $req;    
}


   //supprimer un objet 
public function supprimertesteur($edm)
{
$this->_db->exec("DELETE FROM testeuredm_n WHERE idtesteur='$edm'");
}//fin supprimer*/


//modifier un objet avec son id en parametre 
 public function modifiertesteur($edm) {

	    $req= $this->_db ->prepare("UPDATE testeuredm_n set lieu= :id, ville= :v,nom= :n , idedm_n = :l where idtesteur ='$edm'");

	    	$req-> bindValue(':id',$_POST['identifiant']);
	    	$req-> bindValue(':v',$_POST['lieu']);
	    	$req-> bindValue(':n',$_POST['ville']);
	    	$req-> bindValue(':l',$_POST['nom']);
			$req->execute();
		
		}

		//recherche pour afficher un objet
		 public function cherchertesteur($edm)
{
             $req = array();
	    	$requete = (" SELECT  *  FROM testeuredm_n where idtesteur='$edm' ");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$req[] = new TesteurEdmN ($donnee['idtesteur'],$donnee['lieu'],$donnee['ville'],$donnee['nom'],$donnee['idedm_n']); 
	    	
	    	}
	    	return $req;   
}










	    //affichage ok
	 /*   public function Afficher()
{
             $req = array();
	    	$requete = " SELECT* FROM agent ORDER BY nomagent DESC";
 	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	   $obj= new Agent ($donnee); 
	    	   // $obj->setId_user($donnee['id_user']);
	    	    $req[] = $obj;
	    	}
	    	return $req;
	    		    
}*/
//fonction compter le nombre user ok.
   /*public function compte()
{
     return $this->_db->query('SELECT COUNT(*) FROM agent')->fetchColumn();
}//fin compter
//supprimer un objet 
public function delete($agent)
{
$this->_db->exec("DELETE FROM agent WHERE nummle='$agent'");
}//fin delete*/

//fonction login validate
/*public function login($loguser)
{
	$erreur=null;
$requete = (" SELECT* FROM user where login='$loguser'");
	    	$list = $this->_db->query($requete);
	    	if ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {

	$erreur="<center><font color='red'><h3>Ce Pseudo existe deja choisissez un autre svp!</h3></font></center>";
	 
}//fin if
	return $erreur;	
}
*/

//modifier un objet avec son id en parametre 
 
		//recherche pour afficher un objet
	/*	 public function chercher($agent)
{
             $req = array();
	    	$requete = (" SELECT* FROM agent where nummle='$agent'");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$obj= new Agent ($donnee); 
	    	   // $obj->setId_user($donnee['id_user']);
	    	$req[] =  $obj;
	    	}
	    	return $req;
	    		    
}*/

/*public function chercherlog($num)
{
             $req = array();
	    	$requete = (" SELECT* FROM agent where nummle='$num'");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$obj= new Agent ($donnee); 
	    	   // $obj->setId_user($donnee['id_user']);
	    	$req[] =  $obj;
	    	}
	    	return $req;	    
}*/

/*public function chercherag($ids)
{
     return $this->_db->query("SELECT prenomagent FROM agent where nummle='$ids'")->fetchColumn();
}*///fin recuperer nom service superieur

//===========================authentifier=========================
/*public function connecter ($num,$pass){
	        $req = array();
	    	$requete =("SELECT* FROM agent where(( nummle='$num')  and (password='$pass'))");
	    	
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$obj= new Agent ($donnee); 
	    	   // $obj->setId_user($donnee['id_user']);
	    	$req[] =  $obj;
	    	}
	    	return $req;	
	    	

	    }*///fin fonc

	}

	

?>