var contient = document.getElementById('allcontenu');
var xhr = new XMLHttpRequest();
// Envoi get des form------------------------------------------------------------------
function addcategories()
{
	xhr.open('GET', 'admincontenu/addcategories.php');
	xhr.onreadystatechange = function(){
		if (xhr.readyState== 4 && (xhr.status==200 || xhr.status ==0)) {
			contient.innerHTML = xhr.responseText;
		}
	};
	xhr.send(null);
}
function addcandidats()
{
	xhr.open('GET', 'admincontenu/addcandidats.php');
	xhr.onreadystatechange = function(){
		if (xhr.readyState== 4 && (xhr.status==200 || xhr.status ==0)) {
			contient.innerHTML = xhr.responseText;
		}
	};
	xhr.send(null);
}
function addquestions()
{
	xhr.open('GET', 'admincontenu/addquestions.php');
	xhr.onreadystatechange = function(){
		if (xhr.readyState== 4 && (xhr.status==200 || xhr.status ==0)) {
			contient.innerHTML = xhr.responseText;
		}
	};
	xhr.send(null);
}
// Envoi post des entrées------------------------------------------------------------------
document.getElementById('ajoutcat').addEventListener('click', function(e){
			e.preventDefault();
			sendaddcategories();
		},false);
// fonction ajout des données dans catégories ------------------------------------------------------------------
function sendaddcategories()
{

	var nomcat = document.getElementById('nomcat').value,
		questioncat = document.getElementById('questioncat').value;

	xhr.open('POST', 'admincontenu/addcategories.php');
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.onreadystatechange = function(){
		if (xhr.readyState== 4 && (xhr.status==200 || xhr.status ==0)) {
			contient.innerHTML = xhr.responseText;
		}
	};
	xhr.send('nomcat='+nomcat+'&questioncat='+questioncat);
}
// fonction ajout des données dans candidats------------------------------------------------------------------
function sendaddcandidats()
{

	var nomcand = document.getElementById('nomcand').value,
		agecand = document.getElementById('agecand').value,
		partipolitiq = document.getElementById('partipolitiq').value,
		descripcand = document.getElementById('descripcand').value,
		photocand = document.getElementById('photocand').value;

	xhr.open('POST', 'admincontenu/addcandidats.php');
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.onreadystatechange = function(){
		if (xhr.readyState== 4 && (xhr.status==200 || xhr.status ==0)) {
			contient.innerHTML = xhr.responseText;
		}
	};
	xhr.send('nomcand='+nomcand+'&agecand='+agecand+'&partipolitiq='+partipolitiq+'&descripcand='+descripcand+'&photocand='+photocand);
}

// fonction ajout des données supplementaire des candidats------------------------------------------------------------------
function sendaddsupcandidats()
{

	var candselect = document.getElementById('candselect').value,
		usercand = document.getElementById('usercand').value,
		passcand = document.getElementById('passcand').value,
		facecand = document.getElementById('facecand').value,
		twitcand = document.getElementById('twitcand').value,
		goocand = document.getElementById('goocand').value,
		webcand = document.getElementById('webcand').value,
		descgecand = document.getElementById('descgecand').value,
		infosup = document.getElementById('infosup').value;

	xhr.open('POST', 'admincontenu/addcandidats.php');
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.onreadystatechange = function(){
		if (xhr.readyState== 4 && (xhr.status==200 || xhr.status ==0)) {
			contient.innerHTML = xhr.responseText;
		}
	};
	xhr.send('candselect='+candselect+'&usercand='+usercand+'&passcand='+passcand+'&facecand='+facecand+'&twitcand='+twitcand+'&goocand='+goocand+'&webcand='+webcand+'&descgecand='+descgecand+'&infosup='+infosup);
}
// fonction ajout des données dans reponse candidat------------------------------------------------------------------
function sendaddreponses()
{

	var catquest = document.getElementById('catquest').value,
		candquest = document.getElementById('candquest').value,
		repcand = document.getElementById('repcand').value;

	xhr.open('POST', 'admincontenu/addquestions.php');
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.onreadystatechange = function(){
		if (xhr.readyState== 4 && (xhr.status==200 || xhr.status ==0)) {
			contient.innerHTML = xhr.responseText;
		}
	};
	xhr.send('catquest='+catquest+'&candquest='+candquest+'&repcand='+repcand);
}

// fonction affichage et modif données candidat------------------------------------------------------------------
function printinfcand()
{

	//var catquest = document.getElementById('catquest').value,
	//	candquest = document.getElementById('candquest').value,
	//	repcand = document.getElementById('repcand').value;

	xhr.open('POST', 'admincontenu/consultcandidat.php');
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.onreadystatechange = function(){
		if (xhr.readyState== 4 && (xhr.status==200 || xhr.status ==0)) {
			contient.innerHTML = xhr.responseText;
		}
	};
	//xhr.send('catquest='+catquest+'&candquest='+candquest+'&repcand='+repcand);
	xhr.send(null);
}
// fonction affichage et modif données candidat------------------------------------------------------------------
function printrepcand()
{

	//var catquest = document.getElementById('catquest').value,
	//	candquest = document.getElementById('candquest').value,
	//	repcand = document.getElementById('repcand').value;

	xhr.open('POST', 'admincontenu/reponsecandidat.php');
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.onreadystatechange = function(){
		if (xhr.readyState== 4 && (xhr.status==200 || xhr.status ==0)) {
			contient.innerHTML = xhr.responseText;
		}
	};
	//xhr.send('catquest='+catquest+'&candquest='+candquest+'&repcand='+repcand);
	xhr.send(null);
}

// fonction affichage et modif données candidat------------------------------------------------------------------
function printstatistique()
{

	//var catquest = document.getElementById('jour').value,
	//	candquest = document.getElementById('mois').value,
	//	repcand = document.getElementById('annee').value;

	xhr.open('POST', 'admincontenu/statistique.php');
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.onreadystatechange = function(){
		if (xhr.readyState== 4 && (xhr.status==200 || xhr.status ==0)) {
			contient.innerHTML = xhr.responseText;
		}
	};
	//xhr.send('catquest='+catquest+'&candquest='+candquest+'&repcand='+repcand);
	xhr.send(null);
}

// fonction affichage et modif données candidat------------------------------------------------------------------
function deletetablepage()
{


	xhr.open('POST', 'admincontenu/deletecontain.php');
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.onreadystatechange = function(){
		if (xhr.readyState== 4 && (xhr.status==200 || xhr.status ==0)) {
			contient.innerHTML = xhr.responseText;
		}
	};
	//xhr.send('catquest='+catquest+'&candquest='+candquest+'&repcand='+repcand);
	xhr.send(null);
}

// fonction affichage et modif données candidat------------------------------------------------------------------
/*function modiftablecand()
{


	xhr.open('GET', 'admincontenu/modifcandidat.php?idcand='+);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.onreadystatechange = function(){
		if (xhr.readyState== 4 && (xhr.status==200 || xhr.status ==0)) {
			contient.innerHTML = xhr.responseText;
		}
	};
	//xhr.send('catquest='+catquest+'&candquest='+candquest+'&repcand='+repcand);
	xhr.send(null);
}*/

// fonction affichage et modif données candidat------------------------------------------------------------------
function printstatistiquedate()
{

	var jour = document.getElementById('jour').value,
		mois = document.getElementById('mois').value,
		annee = document.getElementById('annee').value;

	xhr.open('POST', 'admincontenu/statistique.php');
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.onreadystatechange = function(){
		if (xhr.readyState== 4 && (xhr.status==200 || xhr.status ==0)) {
			contient.innerHTML = xhr.responseText;
		}
	};
	//xhr.send('catquest='+catquest+'&candquest='+candquest+'&repcand='+repcand);
	xhr.send('jour='+jour+'&mois='+mois+'&annee='+annee);
}