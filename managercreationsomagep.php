
<?php
require("class_creationsomagep.php");
require("class_creation_testeur_somagep.php");

class Managersomagep
{
private $_db; // Instance de PDO
public function __construct($db)
{
$this->setDb($db);
}


public function setDb(PDO $db)
{
$this->_db = $db;
}
    //Ajouter un compteur
	public function ajoutersomagep(CompteurSomagep $somagep){
	  
	       $req = $this->_db->prepare("
	                     INSERT INTO comptsomagep (`idcomptS`, `ville`, `localisation`, `nom`) 
	                               VALUES (:id,:ville,:lieu,:nom)");
	   
	    	$req-> bindValue(':id',$somagep->getIdCompts());
	    	$req-> bindValue(':ville',$somagep->getVille());
	    	$req-> bindValue(':lieu',$somagep->getLocalisation());
	    	$req-> bindValue(':nom',$somagep->getNom());	 	    	
	    	
	    	$req->execute();
	    		
	    	}


	  // fonction qui permet de verifier si identifiant existe dans la base
	    public function verifier($id)
{
	$erreur=null;
$requete = (" SELECT* FROM comptsomagep where idcomptS='$id'");

	    	$list = $this->_db->query($requete);
	    	if ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {

	$erreur="<center><font color='red'><h3>Ce Compteur Existe deja!</h3></font></center>";
	
}//fin if
	return $erreur;	

}

	  public function Afficher()
{
             $req = array();
	    	$requete = " SELECT* FROM comptsomagep";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $req[]= new CompteurSomagep ($donnee['idcomptS'],$donnee['ville'],$donnee['localisation'],$donnee['nom']); 
	    	   
	    	}
	    	return $req;    
}

   //supprimer un objet 
public function supprimer($somagep)
{
$this->_db->exec("DELETE FROM comptsomagep WHERE idcomptS='$somagep'");
}//fin supprimer*/


//modifier un objet avec son id en parametre 
 public function modifier($somagep) {

	    $req= $this->_db ->prepare("UPDATE comptsomagep set idcomptS= :id, ville= :v, localisation = :l,nom= :n where idcomptS ='$somagep'");

	    	$req-> bindValue(':id',$_POST['identifiant']);
	    	$req-> bindValue(':v',$_POST['lieu']);
	    	$req-> bindValue(':l',$_POST['ville']);
	    	$req-> bindValue(':n',$_POST['nom']);
			$req->execute();
		
		}

		//recherche pour afficher un objet
		 public function chercher($somagep)
{
             $req = array();
	    	$requete = (" SELECT  *  FROM comptsomagep where idcomptS='$somagep' ");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$req[]= new CompteurSomagep ($donnee['idcomptS'],$donnee['ville'],$donnee['localisation'],$donnee['nom']); 
	    	
	    	}
	    	return $req;   
}
//===================================== methodes pour les testeurs===================================
 //Ajouter automatiquement le testeur du compteur qui ete creer
	public function ajoutertesteursomagep(TesteurCompteurSomagep $somagep){
	    	
	       $req = $this->_db->prepare("
	                     INSERT INTO testeurcomptsomagep (`idtesteur`, `ville`, `localisation`, `nom`, `idcomptS`) 
	                               VALUES (:id,:ville,:lieu,:nom,:idtesteur)");

	   		$req-> bindValue(':id',$somagep->getIdtesteur());
	    	$req-> bindValue(':ville',$somagep->getVille());
	    	$req-> bindValue(':lieu',$somagep->getLocalisation());
	    	$req-> bindValue(':nom',$somagep->getNom());
	    	$req-> bindValue(':idtesteur',$somagep->getIdcomptS());	 	    	
	    	
	    	$req->execute();
	    		
	    	}
	  //============================= methodes pour verifier ============================

 public function verifiertesteur($id)
{
	$erreur=null;
$requete = (" SELECT* FROM testeurcomptsomagep where idcomptS='$id'");

	    	$list = $this->_db->query($requete);
	    	if ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {

	$erreur="<center><font color='red'><h3>Pas de testeur creer pour ce Compteur!</h3></font></center>";
	
}//fin if
	return $erreur;	

}


	  public function Affichertesteur()
{
             $req = array();
	    	$requete = " SELECT* FROM testeurcomptsomagep";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	     $req[] = new TesteurCompteurSomagep ($donnee['idtesteur'],$donnee['ville'],$donnee['localisation'],$donnee['nom']
	    	    	,$donnee['idcomptS']); 
	    	}
	    	return $req;    
}


// je recupere de dernier id du compteur
   public function idmax($max)
{
     return $max1=$this->_db->query("SELECT MAX(idcons)  FROM concomptsomagep where idcomptS='$max'")->fetchColumn();

}
// je recupere index de ce compteur

   public function maxindex($max1,$nom)
{
     return $valeur=$this->_db->query("SELECT index1  FROM concomptsomagep where idcons=$max1 and idcomptS='$nom'")->fetchColumn();
}

   //supprimer un objet 
public function supprimertesteur($somagep)
{
$this->_db->exec("DELETE FROM testeurcomptsomagep WHERE idtesteur='$somagep'");
}//fin supprimer*/

//modifier un objet avec son id en parametre 
 public function modifiertesteur($somagep) {

	    $req= $this->_db ->prepare("UPDATE testeurcomptsomagep set ville= :id, localisation= :v, nom = :l,idcomptS= :n where idtesteur ='$somagep'");

	    	$req-> bindValue(':id',$_POST['identifiant']);
	    	$req-> bindValue(':v',$_POST['lieu']);
	    	$req-> bindValue(':l',$_POST['ville']);
	    	$req-> bindValue(':n',$_POST['nom']);
			$req->execute();
		
		}

		//recherche pour afficher un objet
		 public function cherchertesteur($somagep)
{
             $req = array();
	    	$requete = (" SELECT  *  FROM testeurcomptsomagep where idtesteur='$somagep' ");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$req[] = new TesteurCompteurSomagep ($donnee['idtesteur'],$donnee['ville'],$donnee['localisation'],$donnee['nom']
	    	    	,$donnee['idcomptS']); 
	    	
	    	}
	    	return $req;   
}


//=============================== fin des methodes pour les testeurs===================================

	    //affichage ok
	   /* public function Afficher()
{
             $req = array();
	    	$requete = " SELECT* FROM comptsomagep ORDER BY idcomptS DESC";
 	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	   $obj= new Agent ($donnee); 
	    	   // $obj->setId_user($donnee['id_user']);
	    	    $req[] = $obj;
	    	}
	    	return $req;
	    		    
}*/
//fonction compter le nombre user ok.
   /*public function compte()
{
     return $this->_db->query('SELECT COUNT(*) FROM agent')->fetchColumn();
}//fin compter

supprimer un objet 
public function delete($agent)
{
$this->_db->exec("DELETE FROM agent WHERE nummle='$agent'");
}//fin delete*/

//fonction login validate
/*public function login($loguser)
{
	$erreur=null;
$requete = (" SELECT* FROM user where login='$loguser'");
	    	$list = $this->_db->query($requete);
	    	if ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {

	$erreur="<center><font color='red'><h3>Ce Pseudo existe deja choisissez un autre svp!</h3></font></center>";
	 
}//fin if
	return $erreur;	
}
*/

//modifier un objet avec son id en parametre 
 
		//recherche pour afficher un objet
	/*	 public function chercher($agent)
{
             $req = array();
	    	$requete = (" SELECT* FROM agent where nummle='$agent'");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$obj= new Agent ($donnee); 
	    	   // $obj->setId_user($donnee['id_user']);
	    	$req[] =  $obj;
	    	}
	    	return $req;
	    		    
}*/

/*public function chercherlog($num)
{
             $req = array();
	    	$requete = (" SELECT* FROM agent where nummle='$num'");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$obj= new Agent ($donnee); 
	    	   // $obj->setId_user($donnee['id_user']);
	    	$req[] =  $obj;
	    	}
	    	return $req;	    
}*/

/*public function chercherag($ids)
{
     return $this->_db->query("SELECT prenomagent FROM agent where nummle='$ids'")->fetchColumn();
}*///fin recuperer nom service superieur

//===========================authentifier=========================
/*public function connecter ($num,$pass){
	        $req = array();
	    	$requete =("SELECT* FROM agent where(( nummle='$num')  and (password='$pass'))");
	    	
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$obj= new Agent ($donnee); 
	    	   // $obj->setId_user($donnee['id_user']);
	    	$req[] =  $obj;
	    	}
	    	return $req;	
	    	

	    }*///fin fonc

	}

	

?>