
<?php 
 include('config.php');
session_start(); 
?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="img/logo1.png">

  <title>gescompt</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
</head>

<body>

<!-- <h1 class="btn btn-primary">test</h1> -->
<br><br>

   			

  <div class="container content">
      <div class="connex-back">
      <form method="POST" action="authentification.php">
     
        <div class="logodah"><img src="img/logo1.png" width="40%"></div>
        <br>
        <label for="inputText" class="sr-only">Identifiant :</label>
        <!-- <h6 for="" class="" style="text-align:left;">Identifiant :</h6> -->
        <input type="text" name="identifiant" id="inputText" class="form-control col-sm-6 col-md-offset-12" placeholder="Identifiant" required autofocus>
        <br>
        <label for="inputPassword" class="sr-only">Mot de passe</label>
        <!-- <h6 for="" class="" style="text-align:left;">Mot de passe :</h6> -->
        <input type="password" name="motdepasse" id="inputPassword" class="form-control col-sm-6" placeholder="Mot de passe" required>
        <div class="checkbox">
        <!--   <a href="motdepasseoublie.php">Mot de passe oublié</a> -->
        </div><br>


        <button class="btn btn-lg btn-primary btn-block col-sm-6" type="submit" name="Connexion">Connexion</button>
        <br>

        <?php if (isset($_SESSION['message'])) {
              echo '<div class="messageinfos">'.$_SESSION['message'].'</div>';
             
              } ?>
      
      </form>
      <br>

      


     




      </div>
    </div>











 <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

</body>
<?php 
  session_unset();
  session_destroy();
 ?>
</html>