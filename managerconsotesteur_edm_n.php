
<?php
require("class_conso_testeur_edm_n.php");
//require_once("connexion.php");

class ManagerConso_testeur_Edm_n
{
private $_db; // Instance de PDO
public function __construct($db)
{
$this->setDb($db);
}


public function setDb(PDO $db)
{
$this->_db = $db;

}

// pour recuperer identifiant max du consommation edm numerique
   public function idcons()
{
     return $this->_db->query('SELECT MAX(idcons) FROM consedm_n')->fetchColumn();
}
    //Ajouter un compteur

	public function ajouter(ConsoTesteurEdmN $te){
	    	
	       $req = $this->_db->prepare("
	                     INSERT INTO constesteuredm_n (`idconstesteur`, `indexheurecreuse`, `indexheurepleine`, `indexheurepointe`, `heurereleve`, `consommation`, `idtesteur`, `idcons`) 
	                               VALUES (null,:ihc,:ihp,:ihpt,:hr,:con,:idc,:idco)");
	   
	    	/*$req-> bindValue(':id',$ccs->getIdEdm_a());*/
	    	$req-> bindValue(':ihc',$te->getIndexheurecreuse());
	    	$req-> bindValue(':ihp',$te->getIndexheurepleine());
	    	$req-> bindValue(':ihpt',$te->getIndexheurepointe());
	    	/*$req-> bindValue(':dr',$te->getDateReleve());*/
	    	$req-> bindValue(':hr',$te->getHeureReleve());
	    	$req-> bindValue(':con',$te->getConsommation());
	    	$req-> bindValue(':idc',$te->getIdtesteur());
	    	$req-> bindValue(':idco',$te->getIdcons());		    	
	    	
	    	$req->execute(); 

	    }


 public function Afficher()
{
             $req = array();
	    	$requete = " SELECT* FROM constesteuredm_n";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $obj = new ConsoTesteurEdmN ($donnee['indexheurecreuse'],$donnee['indexheurepleine'],$donnee['indexheurepointe'],$donnee['heurereleve'],$donnee['dateheurereleve'],$donnee['consommation'],$donnee['idtesteur']); 
	    	    $obj->setIdconstesteur($donnee['idconstesteur']);
	    	    $req[]=$obj;
	    	}
	    	return $req;    
}


   //supprimer un objet 
public function supprimer($edm)
{
$this->_db->exec("DELETE FROM constesteuredm_n WHERE idconstesteur=".$edm);
}//fin supprimer*/


//modifier un objet avec son id en parametre 
 public function modifier($somagep) {

	    $req= $this->_db ->prepare("UPDATE constesteuredm_n set indexheurecreuse= :hc, indexheurepleine= :hp, indexheurepointe = :hpt,heurereleve= :hr,consommation=(:hc+:hp+:hpt) ,idtesteur= :idt where idconstesteur =".$somagep);

	    	$req-> bindValue(':hc',$hc=$_POST['heurecreuse']);
	    	$req-> bindValue(':hp',$hp=$_POST['heurepleine']);
	    	$req-> bindValue(':hpt',$hpt=$_POST['heurepointe']);
	    	/*$req-> bindValue(':dr',$_POST['datereleve']);*/
	    	$req-> bindValue(':hr',$_POST['heurereleve']);
	    	$req-> bindValue(':idt',$_POST['idt']);

			$req->execute();
		
		}

		//recherche pour afficher un objet
		 public function chercher($somagep)
{
             $req = array();
	    	$requete = (" SELECT  *  FROM constesteuredm_n where idconstesteur=".$somagep);
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$req[]= new ConsoTesteurEdmN ($donnee['indexheurecreuse'],$donnee['indexheurepleine'],$donnee['indexheurepointe'],$donnee['heurereleve'],$donnee['dateheurereleve'],$donnee['consommation'],$donnee['idtesteur']); 
	    	   
	    	
	    	}
	    	return $req;   
}



 public function AfficherDonner($cons)
{
             $req = array();
	    $requete = (" SELECT * from consedm_n,constesteuredm_n where  consedm_n.idcons=constesteuredm_n.idcons and consedm_n.idedm_n='$cons'");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $obj = new ConsoTesteurEdmN ($donnee['indexheurecreuse'],$donnee['indexheurepleine'],$donnee['indexheurepointe'],$donnee['heurereleve'],$donnee['dateheurereleve'],$donnee['consommation'],$donnee['idtesteur'],$donnee['idcons']); 
	    	    $obj->setIdconstesteur($donnee['idconstesteur']);
	    	    $req[]=$obj;
	    	}
	    	return $req;    
}

// methode pour chercher le compteur qui est liee testeur edm numerique
 public function AfficherCons($code)
{
             $req = array();
	    	$requete = (" SELECT* FROM consedm_n where idcons=".$code);
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	   $req[] = new ConsoEdmN ($donnee['idcons'],$donnee['indexheurecreuse'],$donnee['indexheurepleine'],$donnee['indexheurepoint'],$donnee['datereleve'],$donnee['heurereleve'],$donnee['dateheurereleve'],$donnee['consommation'],$donnee['idedm_n']); 
	    	   
	    	}
	    	return $req;    
}




	    //affichage ok
	/*    public function Affichercompteur()
{

			 $req = array();
	    	$requete = " SELECT* FROM comptsomagep ";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $obj = new CompteurSomagep ($donnee['ville'],$donnee['localisation'],$donnee['nom']); 
	    	    $obj->setgetIdCompteur($donnee['idcomptS']);
	    	    $req[]=$obj;
	    	}
	    	return $req;    
}*/
//fonction compter le nombre user ok.
   /*public function compte()
{
     return $this->_db->query('SELECT COUNT(*) FROM agent')->fetchColumn();
}//fin compter
//supprimer un objet 
public function delete($agent)
{
$this->_db->exec("DELETE FROM agent WHERE nummle='$agent'");
}//fin delete*/

//fonction login validate
/*public function login($loguser)
{
	$erreur=null;
$requete = (" SELECT* FROM user where login='$loguser'");
	    	$list = $this->_db->query($requete);
	    	if ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {

	$erreur="<center><font color='red'><h3>Ce Pseudo existe deja choisissez un autre svp!</h3></font></center>";
	 
}//fin if
	return $erreur;	
}
*/

//modifier un objet avec son id en parametre 
 
		//recherche pour afficher un objet
	/*	 public function chercher($agent)
{
             $req = array();
	    	$requete = (" SELECT* FROM agent where nummle='$agent'");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$obj= new Agent ($donnee); 
	    	   // $obj->setId_user($donnee['id_user']);
	    	$req[] =  $obj;
	    	}
	    	return $req;
	    		    
}*/

/*public function chercherlog($num)
{
             $req = array();
	    	$requete = (" SELECT* FROM agent where nummle='$num'");
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$obj= new Agent ($donnee); 
	    	   // $obj->setId_user($donnee['id_user']);
	    	$req[] =  $obj;
	    	}
	    	return $req;	    
}*/

/*public function chercherag($ids)
{
     return $this->_db->query("SELECT prenomagent FROM agent where nummle='$ids'")->fetchColumn();
}*///fin recuperer nom service superieur

//===========================authentifier=========================
/*public function connecter ($num,$pass){
	        $req = array();
	    	$requete =("SELECT* FROM agent where(( nummle='$num')  and (password='$pass'))");
	    	
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	$obj= new Agent ($donnee); 
	    	   // $obj->setId_user($donnee['id_user']);
	    	$req[] =  $obj;
	    	}
	    	return $req;	
	    	

	    }*///fin fonc

	}

	

?>