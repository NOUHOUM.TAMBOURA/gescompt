<?php 

class Suivie

{
	Private $idSuivie;
	private $kilometrage;
	private $dateSv;
	private $idVehicule;

	

public function __construct($id,$k,$d,$idv){
		$this->idSuivie =$id;
		$this->kilometrage =$k;
		$this->dateSv =$d;
		$this->idVehicule =$idv;
	}


	// Les getters
	public function getIdSuivie(){return $this->idSuivie;}
	public function getKilometrage(){return $this->kilometrage;}
	public function getDateSv(){return $this->dateSv;}
	public function getIdVehicule(){return $this->idVehicule;}
	
 
	// les setteurs
	public function setIdSuivie($idSuivie)
	{
		$this->idSuivie= $idSuivie;
	}
	public function setKilometrage($kilometrage)
	{
		$this->kilometrage = $kilometrage;
		
	}
	public function setDateSv($dateSv)
	{

		$this->dateSv = $dateSv;
		
	}
	public function setIdVehicule($idVehicule)
	{

		$this->idVehicule = $idVehicule;
	}


}


 ?>