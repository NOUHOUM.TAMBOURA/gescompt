<?php 

class ConsoTesteurEdmN

{
	Private $idconstesteur;
	private $indexheurecreuse;
	private $indexheurepleine;
	private $indexheurepointe;
	private $dateReleve;
	private $heureReleve;
	private $dateheurereleve;
	private $consommation;
	private $idtesteur;
	private $idcons;
	
	


	public function  __construct($indexhc,$indexhp,$indexhpt,$heurereleve,$dhr,$consommation,$id,$idcons){
		$this->indexheurecreuse =$indexhc;
		$this->indexheurepleine =$indexhp;
		$this->indexheurepointe =$indexhpt;
		/*$this->dateReleve =$datereleve;*/
		$this->heureReleve =$heurereleve;
		$this->dateheurereleve =$dhr;
		$this->consommation =$consommation;
		$this->idtesteur =$id;
		$this->idcons =$idcons;
	}



	// Les getters
	public function getIdconstesteur(){return $this->idconstesteur;}
	public function getIndexheurecreuse(){return $this->indexheurecreuse;}
	public function getIndexheurepleine(){return $this->indexheurepleine;}
	public function getIndexheurepointe(){return $this->indexheurepointe;} 
	/*public function getDateReleve(){return $this->dateReleve;} */
	public function getHeureReleve(){return $this->heureReleve;} 
	public function getDateHeureReleve(){return $this->dateheurereleve;} 
	public function getConsommation(){return $this->consommation;}
	public function getIdtesteur(){return $this->idtesteur;}
	public function getIdcons(){return $this->idcons;}  
	
 
	// les setteurs
	public function setIdconstesteur($idconstesteur)
	{
		$idconstesteur = (int)$idconstesteur;
		$this->idconstesteur = $idconstesteur;
	}
	

	public function setIndexheurecreuse($indexheurecreuse)
	{
		$this->indexheurecreuse = $indexheurecreuse;
	}

	public function setIndexheurepleine($indexheurepleine)
	{
		$this->indexheurepleine = $indexheurepleine;
	}

	public function setIndexheurepointe($indexheurepointe)
	{
		$this->indexheurepointe = $indexheurepointe;
	}

	/*public function setDateReleve($dateReleve)
	{
		$this->dateReleve = $dateReleve;
	
	}*/
	public function setDateHeureReleve($dateheurereleve)
	{
		$this->dateheurereleve = $dateheurereleve;
	
	}

	public function setHeureReleve($heureReleve)
	{
		$this->heureReleve = $heureReleve;
	
	}


	public function setConsommation($consommation)
	{
		$this->consommation = $consommation;
	
	}

	public function setIdtesteur($idtesteur)
	{
		$this->idtesteur = $idtesteur;
	
	}

	public function setIdcons($idcons)
	{
		$this->idcons = $idcons;
	
	}
	
	

}


 ?>