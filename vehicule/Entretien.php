<?php 

class Entretien

{
	Private $identretien;
	private $nombreEntretien;
	private $dateE;
	private $montant;
	private $idVehicule;
	

public function __construct($id,$n,$de,$m,$idv){
		$this->identretien =$id;
		$this->nombreEntretien =$n;
		$this->dateE =$de;
		$this->montant =$m;
		$this->idVehicule =$idv;
	}


	// Les getters
	public function getIdentretien(){return $this->identretien;}
	public function getNombreEntretien(){return $this->nombreEntretien;}
	public function getDateE(){return $this->dateE;}
	public function getMontant(){return $this->montant;}
	public function getIdVehicule(){return $this->idVehicule;} 
	
 
	// les setteurs
	public function setIdentretien($identretien)
	{
		$this->identretien= $identretien;
	}
	public function setNombreEntretien($nombreEntretien)
	{
		$this->nombreEntretien = $nombreEntretien;
		
	}
	public function setDateE($dateE)
	{

		$this->dateE = $dateE;
		
	}
	public function setMontant($montant)
	{

		$this->montant = $montant;
	}
	public function setIdVehicule($idVehicule)
	{

		$this->idVehicule = $idVehicule;
	}
	

}


 ?>