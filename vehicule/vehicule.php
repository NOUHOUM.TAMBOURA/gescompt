<?php 

class Vehicule

{
	Private $idVehicule;
	private $plaque;
	private $modele;
	private $type;
	private $numeroSerie;
	private $proprietaire;
	

public function __construct($id,$p,$m,$t,$n,$p){
		$this->idVehicule =$id;
		$this->plaque =$p;
		$this->modele =$m;
		$this->type =$t;
		$this->numeroSerie =$n;
		$this->proprietaire =$p;
	}


	// Les getters
	public function getIdVehicule(){return $this->idVehicule;}
	public function getPlaque(){return $this->plaque;}
	public function getModele(){return $this->modele;}
	public function getType(){return $this->type;}
	public function getNumeroSerie(){return $this->numeroSerie;}
	public function getProprietaire(){return $this->proprietaire;} 
	
 
	// les setteurs
	public function setIdVehicule($idVehicule)
	{
		$this->idVehicule= $idVehicule;
	}
	public function setPlaque($plaque)
	{
		$this->plaque = $plaque;
		
	}
	public function setModele($modele)
	{

		$this->modele = $modele;
		
	}
	public function setType($type)
	{

		$this->type = $type;
	}
	public function setNumeroSerie($numeroSerie)
	{

		$this->numeroSerie = $numeroSerie;
	}
	public function setProprietaire($proprietaire)
	{

		$this->proprietaire = $proprietaire;
	}
	

}
//====================================================vehicule manager =======================================================

class ManagerVehicule
{
private $_db; // Instance de PDO
public function __construct($db)
{
$this->setDb($db);
}


public function setDb(PDO $db)
{
$this->_db = $db;
}
    //Ajouter un compteur
	public function ajoutervehicule(Vehicule $vehicule){
	  
	       $req = $this->_db->prepare("
	                     INSERT INTO vehicule (`idVehicule`, `plaque`, `modele`, `type`, `numeroSerie`, `proprietaire`) 
	                               VALUES (null,:p,:m,:t,:n,:pr)");
	   
	    	$req-> bindValue(':p',$somagep->getPlaque());
	    	$req-> bindValue(':m',$somagep->getModele());
	    	$req-> bindValue(':t',$somagep->getType());
	    	$req-> bindValue(':n',$somagep->getNumeroSerie());
	    	$req-> bindValue(':pr',$somagep->getProprietaire());	 	    	
	    	
	    	$req->execute();
	    		
	    	}


	    	  public function Affichervehicule()
{
             $req = array();
	    	$requete = " SELECT* FROM vehicule";
	    	$list = $this->_db->query($requete);
	    	while ($donnee = $list->fetch(PDO::FETCH_ASSOC)) {
	    	    $req[]= new Vehicule ($donnee['idVehicule'],$donnee['plaque'],$donnee['modele'],$donnee['type'],$donnee['numeroSerie'],$donnee['proprietaire']); 
	    	   
	    	}
	    	return $req;    
}




	}

	

 ?>