<?php
include('autorisation.php');
require('ManagerRequeteJointure.php');
require('Vehicules.php');
 include('config.php');


  if(isset($_GET['listeglobal'])){
   $_SESSION['listeglobal']=$_GET['listeglobal'];
}
 
 $ud=new ManagerRequeteJointure($db);
$afficher= $ud->Afficherglobale($_SESSION['listeglobal']);


foreach ($afficher as $value) {

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
   <link rel="icon" href="img/logo1.png">

  <title>gescompt</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>


          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <!-- <a href="listevehicule.php" style="margin-left: 1600px;" class="btn btn-success"><i class=""></i>Liste Vehicule</a> -->
              <h4 class="m-0 font-weight-bold text-info">
              Liste Suivi d'un Générateur</h4>
            </div>
              <br>
              <fieldset style="margin-left: 20px;">
          <span> <span style="text-transform: uppercase;"> <b>  Numero Plaque :</b> </span> <?php echo $code=$value->getPlaque(); ?> </span> <br>
          <span> <span style="text-transform: uppercase;"> <b>  Modele Vehicule :</b> </span>  <?php echo $code=$value->getModele(); ?> </span> <br>
          <span> <span style="text-transform: uppercase;"> <b>  Type Vehicule :</b> </span> <?php echo $code=$value->getType(); ?>  </span>  <br>
          <span> <span style="text-transform: uppercase;"> <b>  Numero Serie :</b> </span>  <?php echo $code=$value->getNumeroSerie(); ?> </span> <br>
          <span> <span style="text-transform: uppercase;"> <b>  Proprietaire :</b> </span> <?php echo $code=$value->getProprietaire(); ?> </span> <br>  <br>
                  <a href="listevehicule.php"  class="btn btn-success"><i class=""></i>Liste Vehicule</a>

                  </fieldset>

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                      <tr>
                        <!-- <th colspan="5" class="text-center bg-warning text-white">VEHICULE</th> -->
                      <th colspan="3" class="text-center p-3 mb-2 bg-success text-white">ENTRETIEN</th>
                      <th colspan="2" class="text-center p-3 mb-2 bg-info text-white">SUIVI</th>
                      <th colspan="3" class="text-center p-3 mb-2 bg-secondary text-white">CONTROLE</th>
                      </tr>
                      
                    <tr>
                     <!--  <th>PLAQUE</th>
                      <th>MODELE</th>
                      <th>TYPE</th>
                      <th>SERIE</th>
                      <th>PROPRIETAIRE</th> -->

                      <th>NOM ENTRETIEN</th>
                      <th>DATE ENTRETIEN</th>
                      <th>MONTANT ENTRETIEN(FCFA)</th>

                      <th>KILOMETRAGE</th>
                      <th>DATE</th>

                      <th>TYPE</th>
                      <th>DATE DEBUT</th> 
                      <th>DATE FIN</th>
                      
                    </tr>
                  </thead>
                  
                    
                       


                   
                        <?php  $id=$value->getIdVehicule();  ?>
                  <tr>


                     <td>
                      <?php   
                     $nom=$ud->Afficher2entretien($_SESSION['listeglobal']);
                     foreach ($nom as $v) {
                              echo $nom=$v->getNombreEntretien(); echo "<br>";
                     } ?></td>
                      <td><?php   
                     $nom=$ud->Afficher2entretien($_SESSION['listeglobal']);
                     foreach ($nom as $v) {
                              echo $v->getDateE(); echo "<br>";
                     } ?></td>
                      <td><?php   
                     $nom=$ud->Afficher2entretien($_SESSION['listeglobal']);
                     foreach ($nom as $v) {
                              echo $v->getMontant() ;echo "<br>";
                     } ?></td>




                     <td><?php   
                     $nom=$ud->Afficher3suivie($_SESSION['listeglobal']);
                     foreach ($nom as $v) {
                              echo $v->getKilometrage();echo "<br>";
                     } ?></td>
                     <td><?php   
                     $nom=$ud->Afficher3suivie($_SESSION['listeglobal']);
                     foreach ($nom as $v) {
                              echo $v->getDateSv();echo "<br>";
                     } ?></td>


                     <td><?php   
                     $nom=$ud->Afficher1controle($_SESSION['listeglobal']);
                     foreach ($nom as $v) {
                              echo $v->getTypec();echo "<br>";
                     } ?></td>
                     <td><?php   
                     $nom=$ud->Afficher1controle($_SESSION['listeglobal']);
                     foreach ($nom as $v) {
                              echo $v->getDated();echo "<br>";
                     } ?></td>
                     <td><?php   
                     $nom=$ud->Afficher1controle($_SESSION['listeglobal']);
                     foreach ($nom as $v) {
                              echo $v->getDatef();echo "<br>";
                     } ?></td>



                    <?php
      
                             if(isset($_GET['supprimer'])){
                             $id=$_GET['supprimer'];
                              $suprimer=$ud->supprimer($id);
                            ?>
                   <script type="text/javascript">
                    document.location.replace('listesuivigenerateur.php')
                  </script> 

                 
             
                  </tr>


<?php 
}

}

        
?>
    
      
            


                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->


        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- DataTales Example -->
              <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>


          </ul>

        </nav>
        <!-- End of Topbar -->

       
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->



      <!-- Footer -->
      
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>
        <!-- /.container-fluid -->

      </div>

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>
 <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; 2019</span>
          </div>
        </div>
      </footer>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>


  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>




</body>

</html>
