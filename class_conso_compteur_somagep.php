<?php 

class ConsoCompteurSomagep

{
	Private $idconso;
	private $index;
	private $dateReleve;
	private $heureReleve;
	private $dateheurerevele;
	private $consommation;
	private $idCompteur;
	


	public function  __construct($index,$dateReleve,$heureReleve,$dhr,$consommation,$idCompteur){
		$this->index =$index;
		$this->dateReleve =$dateReleve;
		$this->heureReleve =$heureReleve;
		$this->dateheurerevele =$dhr;
		$this->consommation =$consommation;
		$this->idCompteur =$idCompteur;
	}


	// Les getters
	public function getIdconso(){return $this->idconso;}
	public function getIndex(){return $this->index;}
	public function getDateReleve(){return $this->dateReleve;}
	public function getHeureReleve(){return $this->heureReleve;} 
	public function getDateheurerevele(){return $this->dateheurerevele;} 
	public function getIdCompteur(){return $this->idCompteur;} 
	public function getConsommation(){return $this->consommation;} 
	
 
	// les setteurs
	public function setIdConso($idconso)
	{
		$idconso = (int)$idconso;
		$this->idconso = $idconso;
	}
	public function setIndex($index)
	{

	if (is_string($index))
		{
		$this->index = $index;
		}
	}

	public function setDateReleve($dateReleve)
	{
		$this->dateReleve = $dateReleve;
	}

	public function setHeureReleve($heureReleve)
	{
		$this->heureReleve = $heureReleve;
	
	}

	public function setIdCompteur($idCompteur)
	{
		$this->idCompteur = $idCompteur;
	
	}

	public function setConsommation($consommation)
	{
		$this->consommation = $consommation;
	
	}

	public function setDateheurerevele($dhr)
	{
		$this->dhr = $dhr;
	
	}
	
	

}


 ?>