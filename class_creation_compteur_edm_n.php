<?php 

class CompteurEdmN

{
	Private $idEdm_n;
	private $ville;
	private $localisation;
	private $nom;
	


	public function  __construct($id,$ville,$localisation,$nom){
		$this->idEdm_n =$id;
		$this->ville =$ville;
		$this->localisation =$localisation;
		$this->nom =$nom;
	}


	// Les getters
	public function getIdEdm_n(){return $this->idEdm_n;}
	public function getVille(){return $this->ville;}
	public function getLocalisation(){return $this->localisation;}
	public function getNom(){return $this->nom;} 
	
 
	// les setteurs

	public function setIdEdm_n($idEdm_n)
	{

	if (is_string($idEdm_n))
		{
		$this->idEdm_n = $idEdm_n;
		}
	}

	public function setVille($ville)
	{

	if (is_string($ville))
		{
		$this->ville = $ville;
		}
	}
	public function setLocalisation($localisation)
	{

	if (is_string($localisation))
		{
		$this->localisation = $localisation;
		}
	}
	public function setNom($nom)
	{

	if (is_string($nom))
		{
		$this->nom = $nom;
		}
	}
	

}


 ?>