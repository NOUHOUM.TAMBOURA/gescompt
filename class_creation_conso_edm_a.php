<?php 

class ConsoEdmA

{
	Private $idcons;
	Private $index1;
	private $dateReleve;
	private $heureReleve;
	private $dateHeureReleve;
	private $consommation;
	private $idedm_a;
	


	public function  __construct($index,$date,$heure,$drv,$consommation,$id){
		$this->index1 =$index;
		$this->dateReleve =$date;
		$this->heureReleve =$heure;
		$this->dateHeureReleve =$drv;
		$this->consommation =$consommation;
		$this->idedm_a =$id;
	}

	// Les getters
	public function getIdcons(){return $this->idcons;}
	public function getIndex1(){return $this->index1;}
	public function getDateReleve(){return $this->dateReleve;} 
	public function getHeureReleve(){return $this->heureReleve;} 
	public function getDateHeureReleve(){return $this->dateHeureReleve;} 
	public function getConsommation(){return $this->consommation;}
	public function getIdedm_a(){return $this->idedm_a;} 
	
 
	// les setteurs
	public function setIdcons($idcons)
	{
		$idcons = (int)$idcons;
		$this->idcons = $idcons;
	}

	public function setHeureReleve($dateReleve)
	{
		$this->dateReleve = $dateReleve;
	
	}

	public function setIndex1($index)
	{
		$this->index1 = $index;
	
	}

	public function setConsommation($consommation)
	{
		$this->consommation = $consommation;
	
	}

	public function setDateHeureReleve($dateHeureReleve)
	{
		$this->dateHeureReleve = $dateHeureReleve;
	
	}


	public function setIdedm_a($idedm_a)
	{
		$this->idedm_a = $idedm_a;
	
	}
	
	

}

 ?>