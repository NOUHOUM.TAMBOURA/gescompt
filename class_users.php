<?php 

class Users

{
	Private $iduser;
	private $nomuser;
	private $prenomuser;
	private $emailuser;
	private $identifiantuser;
	private $password;
	private $statut;
	private $telephone;
	

	public function  __construct($iduser,$nom,$prenom,$email,$identifiant,$password,$statut,$telephone){
		$this->iduser =$iduser;
		$this->nomuser =$nom;
		$this->prenomuser =$prenom;
		$this->emailuser =$email;
		$this->identifiantuser =$identifiant;
		$this->password =$password;
		$this->statut =$statut;
		$this->telephone =$telephone;
		
	}


	// Les getters
	public function getIduser(){return $this->iduser;}
	public function getNomuser(){return $this->nomuser;}
	public function getPrenomuser(){return $this->prenomuser;}
	public function getEmailuser(){return $this->emailuser;} 
	public function getIdentifiantuser(){return $this->identifiant;} 
	public function getPassword(){return $this->password;} 
	public function getStatut(){return $this->statut;} 
	public function getTelephone(){return $this->telephone;}	
 
	// les setteurs
	public function setIdcons($iduser)
	{
		$iduser = (int)$iduser;
		$this->iduser = $iduser;
	}
	

	public function setNomuser($nomuser)
	{
		$this->nomuser = $nomuser;
	}

	public function setPrenomuser($prenomuser)
	{
		$this->prenomuser = $prenomuser;
	}

	public function setEmailuser($emailuser)
	{
		$this->emailuser = $emailuser;
	}

	
	public function setIdentifiantuser($identifiant)
	{
		$this->identifiant = $identifiant;
	
	}

	public function setPassword($password)
	{
		$this->password = $password;
	
	}


	public function setStatut($statut)
	{
		$this->statut = $statut;
	
	}

	public function setTelephone($telephone)
	{
		$this->telephone = $telephone;
	
	}
	
}
	


 ?>