<?php 
include('autorisation.php'); 
  require('sommeSurAcceuil.php');
 include('config.php');
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="img/logo1.png">

  <title>gescompt</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">
 <!--  <div class="col-offset-1">
    <img src="entete.png">
  </div>
   -->

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon rotate-n-25">
          <i class=""></i>
        </div>
        <div class="sidebar-brand-text mx-3"><?php echo strtoupper($prenom ."<br>" .$nom);?> 

          </div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->

        <?php if ($_SESSION['statut']==1) { ?> 
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th>
       <li class="nav-item active">
        <a class="nav-link" href="utilisateur.php">
          <i class=""></i>
         <span class="text-center"> <h2 class="btn btn-danger col-sm-12">Utilisateur</h2></span></a>
      </li>
      <?php }  ?>


      <!-- gestion parc auto -->
        <?php if ($_SESSION['statut']==4 ||  ($_SESSION['statut']==1)) { ?> 
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th>
       <li class="nav-item active">
        <a class="nav-link" href="ajoutervehicule.php">
          <i class=""></i>
         <span class="text-center"> <h2 class="btn btn-info col-sm-12">Parc Auto</h2></span></a>
      </li>
      <?php }  ?>

         <!-- gestion groupe electrogene -->
        <?php if ($_SESSION['statut']==4 ||  ($_SESSION['statut']==1)) { ?> 
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th>
       <li class="nav-item active">
        <a class="nav-link" href="ajoutergenerateur.php">
          <i class=""></i>
         <span class="text-center"> <h2 class="btn btn-warning col-sm-12">Génerateur</h2></span></a>
      </li>
      <?php }  ?>



      <li class="nav-item active">
        <a class="nav-link" href="index.php">
          <i class=""></i>
         <span class="text-center text-uppercase"> <h2 class="btn btn-info col-sm-12">ACCUEIL </h2></span></a>
      </li>


<?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        GESTION DES COMPTEURS
      </div>

      <?php }  ?>

      <!-- Nav Item - Pages pour la creation des compteurs -->
      <?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
                  

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Creation</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
            <a class="collapse-item btn btn-primary" href="creationsomagep.php">Somagep</a><br>
            <a class="collapse-item btn btn-success" href="creation_edm_numerique.php"> EDM Numérique</a><br>
            <a class="collapse-item btn btn-info" href="creation_edm_analogique.php">EDM Analogique</a><br>
            <a class="collapse-item btn btn-warning" href="cration_electromenager.php"> Electromenager</a>
          </div>
        </div>
      </li>
      <?php }  ?>

<?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
       <hr class="sidebar-divider">
      <!-- Nav Item - pour la consommation des compteurs -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Releve / Index</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
            <a class="collapse-item btn btn-primary" href="consommation_somagep.php">Somagep</a><br>
            <a class="collapse-item btn btn-success" href="consommation_edm_numerique.php">EDM Numérique</a><br>
            <a class="collapse-item btn btn-info" href="consommation_edm_analogique.php">EDM Analogique</a><br>
            <a class="collapse-item btn btn-warning" href="consoelectromenager.php">Electromenager</a>
          </div>
        </div>
      </li>
      <?php }  ?>
      <!-- Divider -->


      <?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 

      <hr class="sidebar-divider">
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Statistique / Conso</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
           <!--  <a class="collapse-item" href="statistiques.php">Statistique</a> -->
            <a class="collapse-item btn btn-primary" href="statistiques_somagep.php">Somagep</a><br>
            <a class="collapse-item btn btn-success" href="statistiques_edm_numerique.php">EDM Numérique</a><br>
            <a class="collapse-item btn btn-info" href="statistiques_edm_analogique.php">EDM Analogique</a>
           <!--  <a class="collapse-item" href="statistiques_electromenager.php">Electromenager</a> -->
           
          </div>
        </div>
      </li>
      <?php }  ?>
  

    

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

    <!--       <img src="img/image1.png" alt="lkgkk" style="width: 100%;"> -->
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Recherche" aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>

                    </div>
                  </div>
                </form>
              </div>

            </li>
            <!-- <div class="topbar-divider d-none d-sm-block"></div> -->
            
          </ul>
          <?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 

              <h2 style="margin-right: 30%;" class="text-primary">Système Informatique de Gestion des Compteurs</h2>

               <?php }  ?>

               <?php if (($_SESSION['statut']==4)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 

              <h2 style="margin-right: 30%;" class="text-primary">Système Informatique de Gestion Parc Auto et Générateur</h2>

               <?php }  ?>

              <a href="deconnexion.php"><button type="button" class="btn btn-danger">Deconnexion</button></a>

        </nav>


        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
     <!-- ================================================================menu pour compteur====================================================================== -->
          <!-- Content Row 
                  menu pour les compteur  
          -->

          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                           <a href="listecreationsomagep.php"><button type="button" class="btn btn-primary">Somagep</button></a>
                         </div>
                     <?php 
                        $ud=new managersomme($db);
                        $compteur= $ud->sommeSomagep();
                         $testeur= $ud->sommetesteurSomagep();
                         $nombre= $ud->nombreCompteur();
                        echo " <span class='text-primary'>Nombre de Compteur :</span>" ." ".$nombre; echo "<br>";
                        echo " <span class='text-primary'>Consommation Compteur :</span>"." ".$compteur; echo "<br>";
                       


                      ?>
                    </div>
                    <div class="col-auto">
                     <!--  <i class="fas fa-calendar fa-2x text-gray-300"></i> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php }  ?>

            <!-- Earnings (Monthly) Card Example -->
            <?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                         <a href="listeedm_n.php"><button type="button" class="btn btn-success">EDM Numerique</button></a></div>
                        <?php 
                        $ud=new managersomme($db);
                        $compteur= $ud->sommeEdm_n();
                         $testeur= $ud->sommetesteurEdm_n();
                         $nombre= $ud->nombreEdm_n();
                        echo " <span class='text-success'>Nombre de Compteur :</span>" ." ".$nombre; echo "<br>";
                        echo " <span class='text-success'>Consommation Compteur :</span>"." ".$compteur; echo "<br>";
                        echo " <span class='text-success'>Consommation Power-metter :</span>" ." ".$testeur; 


                      ?>
                    </div>
                    <div class="col-auto">
                      <!-- <i class="fas fa-dollar-sign fa-2x text-gray-300"></i> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php }  ?>

            <!-- Earnings (Monthly) Card Example -->
            <?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                        <a href="listeedm_a.php"><button type="button" class="btn btn-info">EDM Analogique</button></a></div>
                      <div class="row no-gutters align-items-center">
                        <div class="col-auto">
                            <?php 
                        $ud=new managersomme($db);
                         $compteur= $ud->sommeEdm_a();
                         $testeur= $ud->sommetesteurEdm_a();
                         $nombre= $ud->nombreEdm_a();
                        echo " <span class='text-info'>Nombre de Compteur :</span>" ." ".$nombre; echo "<br>";
                        echo " <span class='text-info'>Consommation Total :</span>"." ".$compteur; echo "<br>";
                        echo " <span class='text-info'>Total Power-metter :</span>" ." ".$testeur; 


                      ?>
                        </div>
                        <div class="col">
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                     <!--  <i class="fas fa-clipboard-list fa-2x text-gray-300"></i> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php }  ?>

            <!-- Pending Requests Card Example -->
            <?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2) ||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                       <a href="listecreationelectromenager.php"><button type="button" class="btn btn-warning">Electromenager</button></a></div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                            <?php 
                        $ud=new managersomme($db);
                         $compteur= $ud->sommeElectro();
                         $nombre= $ud->nombreElectro();
                         echo "<br>";
                        echo " <span class='text-warning'>Somme Total :</span>" ." ".$compteur;
                         echo "<br>";
                        echo " <span class='text-warning'>Nombre d'ppareil :</span>" ." ".$nombre;
                          
                       

                      ?>
                    </div>
                    <div class="col-auto">
                      <!-- <i class="fas fa-comments fa-2x text-gray-300"></i> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php }  ?>
          </div>

          <!-- ================================================================fin menu pour compteur ============================================================ -->


           <!-- ==================================================menu pour parc auto et generateur============================================================== -->
          <!-- Content Row  -->

          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <?php if (($_SESSION['statut']==4)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
            <div class="col-xl-3 col-md-6 mb-4" style="margin:0px 800px 0px 20px;">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                        <a href="listevehicule.php"><button type="button" class="btn btn-info">Vehicule</button></a>
                       </div>
                     <?php 
                        $ud=new managersomme($db);
                        $nombre= $ud->nombreVehicule();
                        /* $testeur= $ud->sommetesteurSomagep();
                         $nombre= $ud->nombreCompteur();*/
                        echo " <span class='text-info'><h5>Nombre de Vehicule :</h5></span>" ." ".$nombre; echo "<br>";
                        /*echo " <span class='text-primary'>Consommation Compteur :</span>"." ".$compteur; echo "<br>";*/
                       


                      ?>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php }  ?>



            <!-- Earnings (Monthly) Card Example -->
            <?php if (($_SESSION['statut']==4)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                        <a href="listegenerateur.php"><button type="button" class="btn btn-warning">Générateur</button></a>
                        </div>
                        <?php 
                        $ud=new managersomme($db);
                        $nombre= $ud->nombreGeneteur();
                         /*$testeur= $ud->sommetesteurEdm_n();
                         $nombre= $ud->nombreEdm_n();*/
                        echo " <span class='text-warning'> <h5>Nombre de Compteur :</h5></span>" ." ".$nombre; echo "<br>";
                  /*      echo " <span class='text-success'>Consommation Compteur :</span>"." ".$compteur; echo "<br>";
                        echo " <span class='text-success'>Consommation Power-metter :</span>" ." ".$testeur; */


                      ?>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php }  ?>
            
          </div>

          <!-- =========================================================fin pour parc auto et générateur ===================================================== -->

          <!-- Content Row -->

          <br><br>

          <div class="row">

            <!-- Area Chart -->
            <?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2)||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <div class="col-lg-6 mb-4">

              <!-- Illustrations -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary text-uppercase">Malivision</h6>
                </div>
                <div class="card-body">
                  <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="img/undraw_posting_photo.svg" alt="">
                  </div>
                  <p class="text-center text-primary"></p>
          
                </div>
              </div>

            
            </div>
          </div> 
            </div>

            <?php }  ?>

            <!-- ===============================================pour parc auto generateur ============================================ -->

                      <?php if (($_SESSION['statut']==4)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4">
                <div class="col-lg-12 mb-4">

              <!-- Illustrations -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h3 class="m-0 font-weight-bold text-primary text-center text-uppercase">Malivision gestion parc auto et Généteur</h3>
                </div>
                <div class="card-body">
                  <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="img/undraw_posting_photo.svg" alt="">
                  </div>
                  <p class="text-center text-primary"></p>
          
                </div>
              </div>

            
            </div>
          </div> 
            </div>

            <?php }  ?>

<!-- =============================================== fin pour parc auto generateur ============================================ -->
            <!-- Pie Chart -->
            <?php if (($_SESSION['statut']==1) ||  ($_SESSION['statut']==2)||  ($_SESSION['statut']==3)) { ?>  
            <th> <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> </th> 
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Statistiques :</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header"></div>
                      <a class="dropdown-item" href="StatistiquesSomagep.php" style="color: red;">
                      Somagep :</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="StatistiquesEDM_N.php" style="color: red;">
                       EDM Numérique :</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="StatistiquesEDM_A.php" style="color: red;">
                       EDM Analogique :</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="StatistiquesElectro.php" style="color: red;">
                       Appareil Electromenager :</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                    <canvas id="myPieChart"></canvas>
                  </div>
                  <div class="mt-4 text-center small">
                    <span class="mr-2">
                      <i class="fas fa-circle text-primary"></i> 
                    </span>
                    <span class="mr-2">
                      <i class="fas fa-circle text-success"></i> 
                    </span>
                    <span class="mr-2">
                      <i class="fas fa-circle text-info"></i>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <?php }  ?>
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Content Column -->
            <div class="col-lg-6 mb-4">

             
            </div>

            

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span class="btn btn-secondary">Copyright XPERTPRO 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>

</body>

</html>
