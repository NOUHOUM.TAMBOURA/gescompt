   <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon rotate-n-25">
          <i class=""></i>
        </div>
        <div class="sidebar-brand-text mx-3">Nouhoum TAMBOURA </div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="index.php">
          <i class=""></i>
         <span class="text-center text-uppercase"> <h2>ACCUEIL </h2></span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        GESTION DES COMPTEURS
      </div>

      <!-- Nav Item - Pages pour la creation des compteurs -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Creation</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
            <a class="collapse-item" href="creationsomagep.php">Somagep</a>
            <a class="collapse-item" href="creation_edm_numerique.php"> EDM Numérique</a>
            <a class="collapse-item" href="creation_edm_analogique.php">
            EDM Analogique</a>
           <!--  <a class="collapse-item" href="cration_electromenager.php">
            Electromenager</a> -->
          </div>
        </div>
      </li>
       <hr class="sidebar-divider">
      <!-- Nav Item - pour la consommation des compteurs -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Consommation</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
            <a class="collapse-item" href="consommation_somagep.php">Somagep</a>
            <a class="collapse-item" href="consommation_edm_numerique.php">EDM Numérique</a>
            <a class="collapse-item" href="consommation_edm_analogique.php">EDM Analogique</a>
            <a class="collapse-item" href="cration_electromenager.php">Electromenager</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
     <!--  <div class="sidebar-heading">
        Addons
      </div> -->

      <!-- Nav Item - Pages pour les statistiques-->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Statistique</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
            <a class="collapse-item" href="statistiques.php" style="color: blue;">Statistique</a>
            <!-- <a class="collapse-item" href="consommation_somagep.php">Somagep</a>
            <a class="collapse-item" href="consommation_edm_numerique.php">EDM Numérique</a>
            <a class="collapse-item" href="consommation_edm_analogique.php">EDM Analogique</a>
            <a class="collapse-item" href="cration_electromenager.php">Electromenager</a> -->
           
          </div>
        </div>
      </li>

    

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>