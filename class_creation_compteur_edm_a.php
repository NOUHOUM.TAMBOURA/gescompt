<?php 

class CompteurEdmA

{
	Private $idEdm_a;
	private $ville;
	private $localisation;
	private $nom;
	


	public function  __construct($id,$ville,$localisation,$nom){
		$this->idEdm_a =$id;
		$this->ville =$ville;
		$this->localisation =$localisation;
		$this->nom =$nom;
	}


	// Les getters
	public function getIdEdm_a(){return $this->idEdm_a;}
	public function getVille(){return $this->ville;}
	public function getLocalisation(){return $this->localisation;}
	public function getNom(){return $this->nom;} 
	
 
	// les setteurs

	public function setIdEdm_a($idEdm_a)
	{

	if (is_string($idEdm_a))
		{
		$this->idEdm_a = $idEdm_a;
		}
	}
	
	public function setVille($ville)
	{

	if (is_string($ville))
		{
		$this->ville = $ville;
		}
	}
	public function setLocalisation($localisation)
	{

	if (is_string($localisation))
		{
		$this->localisation = $localisation;
		}
	}
	public function setNom($nom)
	{

	if (is_string($nom))
		{
		$this->nom = $nom;
		}
	}
	

}


 ?>